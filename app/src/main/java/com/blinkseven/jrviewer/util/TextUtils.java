package com.blinkseven.jrviewer.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

public class TextUtils {

    public static CharSequence trimTrailingWhitespace(CharSequence source) {
        int length = source.length();
        // Loop back to the first non-whitespace character
        while (--length >= 0 && Character.isWhitespace(source.charAt(length))) {}
        return source.subSequence(0, length + 1);
    }

    public static String makeTagLink(String title) {
        String prefix = title.charAt(0) == '#' ? "" : "#";
        String format = "<a title=\"%1$s%2$s\" href=\"http://joyreactor.cc/tag/%2$s\">%1$s%2$s</a>";
        return String.format(format, prefix, title);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }
}