package com.blinkseven.jrviewer.util;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class ScrollAwareFABBehavior extends FloatingActionButton.Behavior {
    private boolean hide = false;
    private int childHeight;
    private int marginBottom = 0;

    public ScrollAwareFABBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout,
                                       FloatingActionButton child, View directTargetChild,
                                       View target, int nestedScrollAxes) {
        childHeight = child.getHeight();
        ViewGroup.LayoutParams lp = child.getLayoutParams();
        if (lp instanceof ViewGroup.MarginLayoutParams) {
            marginBottom = ((ViewGroup.MarginLayoutParams) lp).bottomMargin;
        }
        return (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child,
                               View target, int dxConsumed, int dyConsumed, int dxUnconsumed,
                               int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed,
                dyUnconsumed);

        if (dyConsumed > 0 && !hide) {
            hideView(child);
            hide = true;
        } else if (dyConsumed < 0 && hide) {
            showView(child);
            hide = false;
        }
    }

    private void hideView(final View child) {
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(child, "translationY", 0, childHeight + marginBottom);
        animator.setDuration(200);
        animator.start();
    }

    private void showView(final View child) {
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(child, "translationY", childHeight + marginBottom, 0);
        animator.setDuration(200);
        animator.start();
    }
}