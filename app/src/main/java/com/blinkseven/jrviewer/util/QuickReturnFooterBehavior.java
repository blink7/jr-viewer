package com.blinkseven.jrviewer.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

public class QuickReturnFooterBehavior extends CoordinatorLayout.Behavior<View> {
    private int totalDyDistance;
    private boolean hide = false;
    private int childHeight;

    public QuickReturnFooterBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout,
                                       @NonNull View child, @NonNull View directTargetChild,
                                       @NonNull View target, int axes, int type) {
        childHeight = child.getHeight();
        return (axes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child,
                               @NonNull View target, int dxConsumed, int dyConsumed,
                               int dxUnconsumed, int dyUnconsumed, int type) {
        if (dyConsumed > 0 && !hide) {
            hideView(child);
            hide = true;
        } else if (dyConsumed < 0 && hide) {
            showView(child);
            hide = false;
        }
    }

    private void hideView(final View child) {
        child.animate().setDuration(300).translationY(childHeight);
    }

    private void showView(final View child) {
        child.animate().setDuration(300).translationY(0);
    }
}