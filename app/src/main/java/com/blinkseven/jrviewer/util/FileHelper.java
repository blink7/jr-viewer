package com.blinkseven.jrviewer.util;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;

import com.blinkseven.jrviewer.R;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FileHelper {

    public static File createTempImageFile(Context context, String uri) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        String imageFileName = "image_" + timeStamp + "_";

        int dotIndex = uri.lastIndexOf('.');
        String sufix = uri.substring(dotIndex, uri.length());

        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                sufix,         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public static File createPublicImageFile(String uri) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        String imageFileName = "image_" + timeStamp + "_";

        int dotIndex = uri.lastIndexOf('.');
        String sufix = uri.substring(dotIndex, uri.length());

        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                sufix,         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public static void shareImage(final Context context, File file) {
        String extension = FilenameUtils.getExtension(file.getName());
        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(context,
                new String[]{file.toString()}, null,
                (path, uri) -> sendIntent(context, uri, extension));
    }

    private static void sendIntent(Context context, Uri uriToImage, String extension) {
        Intent intent = new Intent(Intent.ACTION_SEND)
                .setType("image/" + extension)
                .putExtra(Intent.EXTRA_STREAM, uriToImage);
        context.startActivity(Intent.createChooser(intent,
                context.getString(R.string.share_message)));
    }
}