package com.blinkseven.jrviewer.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import static com.blinkseven.jrviewer.util.Utils.*;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int left;
    private int top;
    private int bottom;
    private int right;

    /**
     * @param space in DP of all rect
     */
    public SpaceItemDecoration(int space) {
        left = top = bottom = right = dpToPx(space);
    }

    /**
     * @params left, top, bottom, right in DP
     */
    public SpaceItemDecoration(int left, int top, int bottom, int right) {
        this.left = dpToPx(left);
        this.top = dpToPx(top);
        this.bottom = dpToPx(bottom);
        this.right = dpToPx(right);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.left = left;
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.right = right;
        }
        outRect.bottom = bottom;
        outRect.top = top;
    }

    public void setLeft(int left) {
        this.left = dpToPx(left);
    }

    public void setRight(int right) {
        this.right = dpToPx(right);
    }
}