package com.blinkseven.jrviewer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.ui.widget.CellGif;
import com.blinkseven.jrviewer.ui.widget.CellImage;
import com.blinkseven.jrviewer.ui.widget.CellVideo;
import com.blinkseven.jrviewer.ui.widget.zoomable.TestCellGif;

import io.realm.RealmList;

public class AttachmentAdapter
        extends BaseRecyclerAdapter<ImageModel, AttachmentAdapter.ImageHolder> {

    private Context context;

    public AttachmentAdapter(Context context, RealmList<ImageModel> images) {
        super(images);
        this.context = context;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.post_content_wrapper, parent, false);
        return new ImageHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.bind(getItem(position), position);
        addClickListeners(holder, position);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        private RelativeLayout wrapper;

        public ImageHolder(View itemView) {
            super(itemView);
            wrapper = itemView.findViewById(R.id.wrapper);
        }

        public void bind(ImageModel imageModel, int position) {
            wrapper.removeAllViews();
            switch (imageModel.getType()) {
                case IMG:
                    CellImage cellImage = new CellImage(context);
                    cellImage.setContent(getData(), position, CellImage.HORIZONTAL);
                    wrapper.addView(cellImage);
                    break;
                case GIF:
                    TestCellGif cellGif = new TestCellGif(context);
                    cellGif.setContent(getData(), position, CellImage.HORIZONTAL);
                    wrapper.addView(cellGif);
                    break;
                default:
                    CellVideo cellVideo = new CellVideo(context);
                    cellVideo.setContent(imageModel, CellImage.HORIZONTAL);
                    wrapper.addView(cellVideo);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return ((ImageModel) getData().get(position)).getUri().hashCode();
    }
}