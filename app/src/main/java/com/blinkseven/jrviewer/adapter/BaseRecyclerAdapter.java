package com.blinkseven.jrviewer.adapter;

import android.support.v7.widget.RecyclerView;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

public abstract class BaseRecyclerAdapter<T extends RealmModel, S extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<S> {

    private RealmList objects;

    private OnItemClickListener onItemClickListener;

    @SuppressWarnings("unchecked")
    public BaseRecyclerAdapter(RealmList objects) {
        if (objects != null) {
            this.objects = objects;
        }
    }

    @Override
    public int getItemCount() {
        if (objects == null) {
            return 0;
        } else {
            return objects.size();
        }
    }

    @SuppressWarnings("unchecked")
    public T getItem(int index) {
        return (T) objects.get(index);
    }

    public RealmList getData() {
        return objects;
    }

    @SuppressWarnings("unchecked")
    protected void addClickListeners(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(v -> {
            RealmObject item;
            try {
                item = (RealmObject) objects.get(Integer.valueOf(v.getTag().toString()));
            } catch (Exception e) {
                return;
            }

            if (onItemClickListener != null)
                onItemClickListener.onItemClick(item);
        });
    }

    public void swapData() {
        objects = null;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemClickListener<Q extends RealmObject> {
        void onItemClick(Q item);
    }
}