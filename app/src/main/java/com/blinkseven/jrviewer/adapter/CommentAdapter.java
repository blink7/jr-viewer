package com.blinkseven.jrviewer.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blinkseven.jrviewer.db.dao.CommentDao;
import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.ui.widget.CellComment;
import com.blinkseven.jrviewer.ui.widget.CellPost;
import com.blinkseven.jrviewer.util.Utils;

import java.util.HashMap;
import java.util.Map;

import io.realm.RealmObject;

public class CommentAdapter
        extends OffsetRealmRecyclerViewAdapter<RealmObject, RecyclerView.ViewHolder> {

    private final int POST = 0;
    private final int COMMENT = 1;

    private Map<String, String> names = new HashMap<>();

    private PostDataModel postData;

    @SuppressWarnings("unchecked")
    public CommentAdapter(PostDataModel postData) {
        super(CommentDao.getComments(), true);
        this.postData = postData;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? POST : COMMENT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case POST:
                CellPost cellPost = new CellPost(parent.getContext());
                cellPost.setFocusable(true);
                return new PostViewHolder(cellPost);
            default:
                CellComment cellComment = new CellComment(parent.getContext());
                cellComment.setFocusable(true);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, Utils.dpToPx(18));
                cellComment.setLayoutParams(layoutParams);
                cellComment.setOrientation(LinearLayout.HORIZONTAL);
                return new ViewHolder(cellComment);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            ((PostViewHolder) holder).bind(postData);
        } else if (holder instanceof CommentAdapter.ViewHolder) {
            ((CommentAdapter.ViewHolder) holder).bind((CommentModel) getItem(position - 1));
        }
    }

    @Override
    public long getItemId(int index) {
        return index == 0 ? 0 : Long.parseLong(((CommentModel) getData().get(index - 1)).getId());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CellComment cellComment;

        public ViewHolder(CellComment commentView) {
            super(commentView);
            cellComment = commentView;
        }

        public void bind(CommentModel commentModel) {
            names.put(commentModel.getId(), commentModel.getUserName());
            cellComment.setContent(commentModel, names.get(commentModel.getParentId()));
        }
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        private CellPost cellPost;

        public PostViewHolder(CellPost postView) {
            super(postView);
            cellPost = postView;
        }

        public void bind(PostDataModel data) {
            cellPost.setData(data, true);
        }
    }
}