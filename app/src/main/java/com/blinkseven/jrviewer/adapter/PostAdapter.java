package com.blinkseven.jrviewer.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.dao.PostDao;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostFavoriteModel;
import com.blinkseven.jrviewer.db.model.PostFriendlineModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;
import com.blinkseven.jrviewer.ui.widget.CellPost;

import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;

public class PostAdapter
        extends RealmRecyclerViewAdapter<RealmObject, RecyclerView.ViewHolder> {

    private PostType type;

    private OnItemClickListener onItemClickListener;

    @SuppressWarnings("unchecked")
    public PostAdapter(PostType type) {
        super(PostDao.getByType(type), true);
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CellPost cellPost = new CellPost(parent.getContext());
        cellPost.setFocusable(true);
        return new ViewHolder(cellPost);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PostDataModel elements = null;
        switch (type) {
            case GOOD:
                elements = ((PostGoodModel) getItem(position)).getData();
                break;
            case ALL:
                elements = ((PostAllModel) getItem(position)).getData();
                break;
            case BEST:
                elements = ((PostBestModel) getItem(position)).getData();
                break;
            case ABYSS:
                elements = ((PostAbyssModel) getItem(position)).getData();
                break;
            case TAG:
                elements = ((PostTagModel) getItem(position)).getData();
                break;
            case USER:
                elements = ((PostUserModel) getItem(position)).getData();
                break;
            case FAVORITE:
                elements = ((PostFavoriteModel) getItem(position)).getData();
                break;
            case FRIENDLINE:
                elements = ((PostFriendlineModel) getItem(position)).getData();
                break;
        }

        ((PostAdapter.ViewHolder) holder).bind(elements);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CellPost cellPost;

        public ViewHolder(CellPost postView) {
            super(postView);
            cellPost = postView;
        }

        public void bind(PostDataModel data) {
            cellPost.setData(data, false);
            cellPost.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(data.getId());
                }
            });
        }
    }

    @Override
    public long getItemId(int index) {
        switch (type) {
            case GOOD:
                return Long.parseLong(((PostGoodModel) getData().get(index)).getId());
            case ALL:
                return Long.parseLong(((PostAllModel) getData().get(index)).getId());
            case BEST:
                return Long.parseLong(((PostBestModel) getData().get(index)).getId());
            case ABYSS:
                return Long.parseLong(((PostAbyssModel) getData().get(index)).getId());
            case TAG:
                return Long.parseLong(((PostTagModel) getData().get(index)).getId());
            case USER:
                return Long.parseLong(((PostUserModel) getData().get(index)).getId());
            case FAVORITE:
                return Long.parseLong(((PostFavoriteModel) getData().get(index)).getId());
            case FRIENDLINE:
                return Long.parseLong(((PostFriendlineModel) getData().get(index)).getId());
            default:
                return 0L;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}