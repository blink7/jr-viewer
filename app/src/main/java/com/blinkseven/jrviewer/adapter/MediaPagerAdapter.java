package com.blinkseven.jrviewer.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.ui.widget.progress.LoadingProgressBarDrawable;
import com.blinkseven.jrviewer.ui.widget.progress.LoadingProgressBarView;
import com.blinkseven.jrviewer.ui.widget.zoomable.DoubleTapGestureListener;
import com.blinkseven.jrviewer.ui.widget.zoomable.ZoomableDraweeView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.github.piasy.biv.view.BigImageView;

import static com.blinkseven.jrviewer.util.Utils.*;

public class MediaPagerAdapter extends PagerAdapter {

    private String[] uris;

    private LayoutInflater layoutInflater;
    private Context context;

    private OnClickListener clickListener;

    public MediaPagerAdapter(Context context, String[] uris) {
        this.uris = uris;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LoadingProgressBarDrawable progress;
        progress = new LoadingProgressBarDrawable();
        //TODO: Set color.
        progress.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        progress.setBarRadius(dpToPx(24));
        progress.setFontSize(dpToPx(12));
        progress.setBarWidth(dpToPx(4));

        View page;
        if (uris[position].endsWith(".gif")) {
            page = layoutInflater.inflate(R.layout.fragment_gif, container, false);
            ZoomableDraweeView image = page.findViewById(R.id.image);
            image.setIsLongpressEnabled(false);
            image.setTapListener(new DoubleTapGestureListener(image) {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if (clickListener != null) {
                        clickListener.onClick();
                    }
                    return super.onSingleTapConfirmed(e);
                }
            });
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uris[position])
                    .setOldController(image.getController())
                    .setAutoPlayAnimations(true)
                    .build();
            image.setController(controller);
            image.getHierarchy().setProgressBarImage(progress);
        } else {
            page = layoutInflater.inflate(R.layout.fragment_image, container, false);
            BigImageView image = page.findViewById(R.id.image);
            image.getSSIV().setMaxScale(10.0f);
            image.getSSIV().setDoubleTapZoomDuration(300);
            image.setOnClickListener(view -> {
                if (clickListener != null) {
                    clickListener.onClick();
                }
            });
            image.setProgressIndicator(new LoadingProgressBarView(context, progress));
            image.showImage(Uri.parse(uris[position]));
        }

        container.addView(page);
        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return uris != null ? uris.length : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public interface OnClickListener {
        void onClick();
    }

    public void setOnClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }
}