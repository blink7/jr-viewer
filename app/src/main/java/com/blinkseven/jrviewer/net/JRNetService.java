package com.blinkseven.jrviewer.net;

import com.blinkseven.jrviewer.db.model.AuthorizationModel;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.net.response.StatusResponse;
import com.blinkseven.jrviewer.net.response.CommentResponse;
import com.blinkseven.jrviewer.net.response.PostDetailResponse;
import com.blinkseven.jrviewer.net.response.PostsResponse;
import com.blinkseven.jrviewer.net.response.SearchResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface JRNetService {

    @GET("/donate")
    Observable<AuthorizationModel> getAuthorizationToken();

    @FormUrlEncoded
    @POST("/login")
    Observable<StatusResponse> authorization(@Field("signin[username]") String login,
                                             @Field("signin[password]") String password,
                                             @Field("signin[_csrf_token]") String token);

    @GET("/user/{name}")
    Observable<UserModel> getUser(@Path("name") String name);

    @GET("/post/{id}")
    Observable<PostDetailResponse> getPostById(@Path("id") String postId);

    @GET("/{page}")
    Observable<PostsResponse> getGoodPosts(@Path("page") int page);

    @GET("/all/{page}")
    Observable<PostsResponse> getAllPosts(@Path("page") int page);

    @GET("/best/{page}")
    Observable<PostsResponse> getBestPosts(@Path("page") int page);

    @GET("/new/{page}")
    Observable<PostsResponse> getAbyssPosts(@Path("page") int page);

    @GET("/tag/{tag}/{page}")
    Observable<Response<PostsResponse>> getPostsByTag(@Path("tag") String tag, @Path("page") int page);

    @GET
    Observable<PostsResponse> getPostsWithUrl(@Url String url);

    @GET("/user/{name}/{page}")
    Observable<PostsResponse> getPostsByUser(@Path("name") String name, @Path("page") int page);

    @GET("/post/comment/{id}")
    Observable<CommentResponse> getCommentById(@Path("id") String id);

    @GET("/search")
    Call<SearchResponse> getSuggestions(@Query("q") String... keys);

    @GET("/user/{name}/friendline/{page}")
    Observable<PostsResponse> getFriendline(@Path("name") String name, @Path("page") int page);

    @GET("/user/{name}/favorite/{page}")
    Observable<PostsResponse> getFavorite(@Path("name") String name, @Path("page") int page);

    @GET("/settings")
    Observable<Void> getProfileSettings();
}