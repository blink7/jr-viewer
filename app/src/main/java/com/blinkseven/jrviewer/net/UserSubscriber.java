package com.blinkseven.jrviewer.net;

import android.support.annotation.NonNull;
import android.util.Log;

import com.blinkseven.jrviewer.JRViewerApp;
import com.blinkseven.jrviewer.events.OnErrorLoader;
import com.blinkseven.jrviewer.events.OnSuccessEvent;
import com.blinkseven.jrviewer.events.OnSuccessUserProfile;
import com.blinkseven.jrviewer.net.error.AuthorizationException;
import com.blinkseven.jrviewer.util.Utils;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.Disposable;

import static com.blinkseven.jrviewer.util.Utils.executeTransaction;

public class UserSubscriber {

    private static final String TAG = UserSubscriber.class.getSimpleName();

    public static Disposable authorization(@NonNull String login, @NonNull String password) {
        return JRViewerApp.getApiService()
                .getAuthorizationToken()
                .retry(3)
                .subscribe(authorizationModel -> JRViewerApp.getApiService()
                        .authorization(login, password, authorizationModel.getToken())
                        .subscribe(status -> {
                            if (status.getStatus().equals("success")) {
                                JRViewerApp.getApiService().getUser(login)
                                        .subscribe(userModel -> {
                                            userModel.setCurrentUser(true);
                                            userModel.setActiveUser(true);
                                            userModel.setAuthorization(authorizationModel);
                                            executeTransaction(
                                                    realm -> realm.insertOrUpdate(userModel));
                                        });
                            } else {
                                throw new AuthorizationException(status.getStatus());
                            }
                        }, error -> {
                            Log.e(TAG, "Authorization exception for token: "
                                    + authorizationModel.getToken());
                            error.printStackTrace();
                            EventBus.getDefault().post(new OnErrorLoader(error));
                        }, () -> EventBus.getDefault().post(new OnSuccessEvent())),
                        error -> {
                            Log.e(TAG, "Get token exception");
                            error.printStackTrace();
                            EventBus.getDefault().post(new OnErrorLoader(error));
                        });
    }

    public static Disposable loadUserById(@NonNull String userId) {
        return JRViewerApp.getApiService()
                .getUser(userId)
                .retry(3)
                .subscribe(user -> {
                    user.setComplete(true);
                    EventBus.getDefault().post(new OnSuccessUserProfile(user));
                });
    }
}