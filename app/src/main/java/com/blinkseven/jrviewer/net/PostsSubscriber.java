package com.blinkseven.jrviewer.net;

import com.blinkseven.jrviewer.JRViewerApp;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostFavoriteModel;
import com.blinkseven.jrviewer.db.model.PostFriendlineModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.events.OnErrorPostDetail;
import com.blinkseven.jrviewer.events.OnErrorPosts;
import com.blinkseven.jrviewer.events.PostDetailEvent;
import com.blinkseven.jrviewer.events.PostsEvent;
import com.blinkseven.jrviewer.net.response.PostsResponse;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.Disposable;

import static com.blinkseven.jrviewer.util.Utils.*;

public class PostsSubscriber {

    private static final String TAG = PostsSubscriber.class.getSimpleName();

    public static synchronized Disposable loadPostById(String id) {
        return JRViewerApp.getApiService()
                .getPostById(id)
                .retry(3)
                .subscribe(postResult -> {
                    executeTransaction(realm -> {
                        realm.insertOrUpdate(postResult.getPost());
                        if (postResult.getComments() != null) {
                            realm.insertOrUpdate(postResult.getComments());
                        }
                    });
                    EventBus.getDefault().post(new PostDetailEvent());
                }, error -> EventBus.getDefault().post(new OnErrorPostDetail(error)));
    }

    public static synchronized Disposable loadGoodPosts(int page) {
        return JRViewerApp.getApiService()
                .getGoodPosts(page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostGoodModel.class);
                        }
                        realm.insertOrUpdate(postsResult.getPosts(PostType.GOOD));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadAllPosts(int page) {
        return JRViewerApp.getApiService()
                .getAllPosts(page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostAllModel.class);
                        }
                        realm.insertOrUpdate(postsResult.getPosts(PostType.ALL));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadBestPosts(int page) {
        return JRViewerApp.getApiService()
                .getBestPosts(page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostBestModel.class);
                        }
                        realm.insertOrUpdate(postsResult.getPosts(PostType.BEST));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadAbyssPosts(int page) {
        return JRViewerApp.getApiService()
                .getAbyssPosts(page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostAbyssModel.class);
                        }
                        realm.insertOrUpdate(postsResult.getPosts(PostType.ABYSS));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadTagPosts(int page, String tag) {
        return JRViewerApp.getApiService()
                .getPostsByTag(tag, page)
                .retry(3)
                .subscribe(response -> {
                    final PostsResponse postsResult = response.body();
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostTagModel.class);
                        }
                        realm.copyToRealmOrUpdate(postsResult.getPosts(PostType.TAG));
                    });
                    PostsEvent event = new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage());
                    if (response.raw().priorResponse() != null) {
                        event.setRedirectionUrl(response.raw().request().url().toString());
                    }
                    EventBus.getDefault().post(event);
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadTagPostsWithUrl(int page, String url) {
        return JRViewerApp.getApiService()
                .getPostsWithUrl(url + "/" + page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostTagModel.class);
                        }
                        realm.copyToRealmOrUpdate(postsResult.getPosts(PostType.TAG));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadUserPosts(int page, String name) {
        return JRViewerApp.getApiService()
                .getPostsByUser(name, page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostUserModel.class);
                        }
                        realm.copyToRealmOrUpdate(postsResult.getPosts(PostType.USER));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadFavoritePosts(int page, String name) {
        return JRViewerApp.getApiService()
                .getFavorite(name, page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostFavoriteModel.class);
                        }
                        realm.copyToRealmOrUpdate(postsResult.getPosts(PostType.FAVORITE));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }

    public static synchronized Disposable loadFriendlinePosts(int page, String name) {
        return JRViewerApp.getApiService()
                .getFriendline(name, page)
                .retry(3)
                .subscribe(postsResult -> {
                    executeTransaction(realm -> {
                        if (postsResult.getCurrentPage() == postsResult.getTotalPage()) {
                            realm.delete(PostFriendlineModel.class);
                        }
                        realm.copyToRealmOrUpdate(postsResult.getPosts(PostType.FRIENDLINE));
                    });
                    EventBus.getDefault().post(new PostsEvent(
                            postsResult.getCurrentPage(),
                            postsResult.getTotalPage()));
                }, error -> EventBus.getDefault().post(new OnErrorPosts(error)));
    }
}