package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.model.SearchSuggestionModel;

import java.util.List;

public class SearchResponse {
    private List<SearchSuggestionModel> suggestions;

    public List<SearchSuggestionModel> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<SearchSuggestionModel> suggestions) {
        this.suggestions = suggestions;
    }
}