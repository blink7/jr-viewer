package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.PostType;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

public class PostsResponse {
    private int currentPage;
    private int totalPage;
    private List<ResponsePostModel> postsData;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void setPostsData(List<ResponsePostModel> postsData) {
        this.postsData = postsData;
    }

    public List<RealmObject> getPosts(PostType type) {
        List<RealmObject> posts = new ArrayList<>();
        if (postsData != null) {
            for (ResponsePostModel responsePost : postsData) {
                posts.add(responsePost.toPostModel(type));
            }
        }
        return posts;
    }
}