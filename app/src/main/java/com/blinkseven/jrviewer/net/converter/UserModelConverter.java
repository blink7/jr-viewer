package com.blinkseven.jrviewer.net.converter;

import android.support.annotation.NonNull;

import com.blinkseven.jrviewer.db.model.UserAwardModel;
import com.blinkseven.jrviewer.db.model.UserModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.RealmList;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class UserModelConverter implements Converter<ResponseBody, UserModel> {

    static final UserModelConverter INSTANCE = new UserModelConverter();

    @Override
    public UserModel convert(@NonNull ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        UserModel userModel = new UserModel();

        Element sidebarUser = doc.select(".sidebarContent > .user").first();
        if (sidebarUser == null) {
            return userModel;
        }
        userModel.setUserName(sidebarUser.select("span").text());
        userModel.setUserAvatar(sidebarUser.select("img").attr("src"));

        Element userAwards = doc.select(".sidebarContent > .user-awards").first();
        if (userAwards != null && !userAwards.select(".award_holder").isEmpty()) {
            Elements awardHolders = userAwards.select(".award_holder");
            RealmList<UserAwardModel> awards = new RealmList<>();
            for (Element awardHolder : awardHolders) {
                UserAwardModel award = new UserAwardModel();
                award.setAwardUrn(awardHolder.select("a").attr("href"));
                award.setImageUri(awardHolder.select("img").attr("src"));
                award.setDescription(awardHolder.select("img").attr("title"));
                awards.add(award);
            }
            userModel.setAwards(awards);
        }

        Element starRow = doc.select(".sidebarContent > .stars > .star-row-0").first();
        userModel.setStars(starRow.select(".star-0").size());
        Element progressRow = doc.select(".sidebarContent > .stars .poll_res_bg_active").first();
        Matcher matcher = Pattern.compile("\\d+").matcher(progressRow.attr("style"));
        if (matcher.find()) {
            userModel.setProgress(Integer.parseInt(matcher.group()));
        }

        Element ratingRow = doc.select(".sidebarContent > #rating-text").first();
        userModel.setRatingText(ratingRow.select("> a").text());
        userModel.setRatingTotal(ratingRow.select("> b").text());
        userModel.setRatingWeek(ratingRow.select("> div").text());

        Element sidebarContent = doc
                .select(".sidebar_block:has(.sideheader:containsOwn(Профиль))").first()
                .select(".sidebarContent").first();

        Element counters = sidebarContent.select("> p").first();
        String[] profileLines = counters.html().split("<br>");
        matcher = Pattern.compile("\\d+").matcher(profileLines[0]);
        if (matcher.find()) {
            userModel.setTotalPostsNumber(matcher.group());
        }
        matcher = Pattern.compile("\\d+").matcher(profileLines[1]);
        if (matcher.find()) {
            userModel.setGoodPostsNumber(matcher.group());
        }
        matcher = Pattern.compile("\\d+").matcher(profileLines[2]);
        if (matcher.find()) {
            userModel.setBestPostsNumber(matcher.group());
        }
        matcher = Pattern.compile("\\d+").matcher(profileLines[4]);
        if (matcher.find()) {
            userModel.setCommentsNumber(matcher.group());
        }

        Elements dates = sidebarContent.select("> span");
        userModel.setSince(dates.select("> b:containsOwn(С нами с)")
                .first().nextElementSibling().ownText());
        userModel.setLastVisit(dates.select("> b:containsOwn(Последний раз заходил)")
                .first().parent().ownText().replace(": ", ""));
        userModel.setConsecutiveDays(dates.select("> b:containsOwn(Дней подряд)")
                .first().parent().ownText().replace(": ", ""));

        return userModel;
    }
}