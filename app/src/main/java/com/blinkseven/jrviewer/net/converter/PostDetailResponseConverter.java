package com.blinkseven.jrviewer.net.converter;

import android.support.annotation.NonNull;

import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.net.parser.CommentsParser;
import com.blinkseven.jrviewer.net.parser.PostParser;
import com.blinkseven.jrviewer.net.response.PostDetailResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class PostDetailResponseConverter implements Converter<ResponseBody, PostDetailResponse> {
    static final PostDetailResponseConverter INSTANCE = new PostDetailResponseConverter();

    @Override
    public PostDetailResponse convert(@NonNull ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        PostDetailResponse response = new PostDetailResponse();

        response.setPostData(new PostParser().apply(doc.select(".postContainer").first()));

        Element commentsList = doc.select(".comment_list_post").first();
        if (commentsList.select("i:contains(нет комментариев)").size() != 0) {
            return response;
        }

        CommentsParser commentsParser = new CommentsParser();
        List<CommentModel> commentModels = new ArrayList<>();
        for (Element comment : commentsList.select(".comment")) {
            commentModels.add(commentsParser.apply(comment));
        }
        response.setComments(commentModels);

        return response;
    }
}