package com.blinkseven.jrviewer.net.converter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.blinkseven.jrviewer.net.parser.PostParser;
import com.blinkseven.jrviewer.net.response.PostsResponse;
import com.blinkseven.jrviewer.net.response.ResponsePostModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class PostsResponseConverter implements Converter<ResponseBody, PostsResponse> {
    private static final String TAG = PostsResponseConverter.class.getSimpleName();

    static final PostsResponseConverter INSTANCE = new PostsResponseConverter();

    @Override
    public PostsResponse convert(@NonNull ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        PostsResponse response = new PostsResponse();

        PostParser postParser = new PostParser();
        List<ResponsePostModel> postsData = new ArrayList<>();
        if (doc.select(".postContainer").size() != 0) {
            for (Element postContainer : doc.select(".postContainer")) {
                postsData.add(postParser.apply(postContainer));
            }
        }
        response.setPostsData(postsData);

        Elements currentPage = doc.select("div.pagination_expanded > span.current");
        if (currentPage.size() != 0) {
            response.setCurrentPage(Integer.parseInt(currentPage.text()));
        } else {
            response.setCurrentPage(0);
        }

        Elements totalPages = doc.select("div.pagination_expanded");
        if (totalPages.size() != 0) {
            Element pagination = totalPages.first().child(0);
            response.setTotalPage(pagination.tagName().equals("a") ?
                    Integer.parseInt(pagination.text()) : response.getCurrentPage());
        } else {
            response.setTotalPage(0);
        }
        return response;
    }
}