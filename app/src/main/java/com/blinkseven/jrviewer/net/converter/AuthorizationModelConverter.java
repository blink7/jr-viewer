package com.blinkseven.jrviewer.net.converter;

import com.blinkseven.jrviewer.db.model.AuthorizationModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class AuthorizationModelConverter implements Converter<ResponseBody, AuthorizationModel> {

    static final AuthorizationModelConverter INSTANCE = new AuthorizationModelConverter();

    @Override
    public AuthorizationModel convert(ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        return new AuthorizationModel(doc.select("#signin__csrf_token").val());
    }
}