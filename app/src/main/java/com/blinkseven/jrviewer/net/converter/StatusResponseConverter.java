package com.blinkseven.jrviewer.net.converter;

import android.support.annotation.NonNull;

import com.blinkseven.jrviewer.net.response.StatusResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class StatusResponseConverter implements Converter<ResponseBody, StatusResponse> {

    static final StatusResponseConverter INSTANCE = new StatusResponseConverter();

    @Override
    public StatusResponse convert(@NonNull ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        StatusResponse response = new StatusResponse();

        Element errorList = doc.select(".error_list").first();
        if (errorList != null) {
            response.setStatus(errorList.select("li").text());
            return response;
        } else if (!doc.select("#logout").isEmpty()) {
            response.setStatus("success");
            return response;
        } else {
            response.setStatus("error");
            return response;
        }
    }
}