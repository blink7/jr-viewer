package com.blinkseven.jrviewer.net.converter;

import com.blinkseven.jrviewer.db.model.SearchSuggestionModel;
import com.blinkseven.jrviewer.net.response.SearchResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class SearchResponseConverter implements Converter<ResponseBody, SearchResponse> {
    final static SearchResponseConverter INSTANCE = new SearchResponseConverter();

    @Override
    public SearchResponse convert(ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());
        Elements blogResults = doc.select(".blog_results");

        SearchResponse searchResponse = new SearchResponse();

        if (blogResults.isEmpty()) {
            searchResponse.setSuggestions(Collections.emptyList());
            return searchResponse;
        }

        List<SearchSuggestionModel> suggestions = new ArrayList<>();
        for (Element name : blogResults.select("a")) {
            String text = name.text().replaceAll("(\\(\\d+\\))", "").trim();
            if (!text.isEmpty()) {
                suggestions.add(new SearchSuggestionModel(text));
            }
        }
        searchResponse.setSuggestions(suggestions);
        return searchResponse;
    }
}