package com.blinkseven.jrviewer.net;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.blinkseven.jrviewer.JRViewerApp;
import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.SearchSuggestionModel;
import com.blinkseven.jrviewer.net.response.SearchResponse;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import retrofit2.Call;

import static android.app.SearchManager.SUGGEST_URI_PATH_QUERY;

public class SearchSuggestionProvider extends ContentProvider {

    private static final String TAG = SearchSuggestionProvider.class.getSimpleName();

    public static final String AUTHORITY = "com.blinkseven.jrviewer.SearchSuggestionProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/search");

    private static final int SEARCH_SUGGEST = 1;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
        uriMatcher.addURI(AUTHORITY, SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST);
    }

    private static final String[] SEARCH_SUGGEST_COLUMNS = {
            BaseColumns._ID,
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_ICON_1,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA
    };

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings,
                        @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {

        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                MatrixCursor matrixCursor = new MatrixCursor(SEARCH_SUGGEST_COLUMNS);
                String query = uri.getLastPathSegment().toLowerCase();
                if (query.equals(SUGGEST_URI_PATH_QUERY)) {
                    return matrixCursor;
                }

                int index = 0;
                Set<String> matchedSuggestions = new HashSet<>();

                // Search for recent and matched suggestions from DB
                Realm realm = Realm.getDefaultInstance();
                List<SearchSuggestionModel> recentSuggestions
                        = realm.where(SearchSuggestionModel.class).findAll();
                for (SearchSuggestionModel suggestion : recentSuggestions) {
                    if (suggestion.getName().toLowerCase().startsWith(query)) {
                        matchedSuggestions.add(suggestion.getName());
                        matrixCursor.addRow(new String[]{
                                Integer.toString(index++),
                                suggestion.getName(),
                                Integer.toString(R.drawable.ic_query_history),
                                suggestion.getName()});
                    }
                }
                realm.close();

                // Search for internet suggestions
                Call<SearchResponse> call
                        = JRViewerApp.getApiService().getSuggestions(query.split(" "));
                try {
                    SearchResponse response = call.execute().body();
                    List<SearchSuggestionModel> suggestions = response.getSuggestions();
                    for (SearchSuggestionModel suggestion : suggestions) {
                        if (!matchedSuggestions.contains(suggestion.getName())) {
                            matrixCursor.addRow(new String[]{
                                    Integer.toString(index++),
                                    suggestion.getName(),
                                    Integer.toString(R.drawable.ic_search_dark),
                                    suggestion.getName()});
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return matrixCursor;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues,
                      @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                return SearchManager.SUGGEST_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown Uri " + uri);
        }
    }
}