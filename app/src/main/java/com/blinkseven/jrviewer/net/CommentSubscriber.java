package com.blinkseven.jrviewer.net;

import com.blinkseven.jrviewer.JRViewerApp;
import com.blinkseven.jrviewer.db.model.CommentModel;

import io.reactivex.disposables.Disposable;
import io.realm.Realm;

public class CommentSubscriber {

    public static synchronized Disposable loadCommentById(String id) {
        return JRViewerApp.getApiService().getCommentById(id)
                .retry(3)
                .subscribe(commentResult -> {
                    Realm realmInstance = null;
                    try {
                        realmInstance = Realm.getDefaultInstance();
                        realmInstance.executeTransaction(realm -> {
                            CommentModel commentModel = realm
                                    .where(CommentModel.class)
                                    .equalTo("id", id)
                                    .findFirst();
                            commentModel.setText(commentResult.getText());
                            if (commentResult.getImage() != null) {
                                commentModel.setImage(
                                        realm.copyToRealmOrUpdate(commentResult.getImage()));
                            }
                        });
                    } finally {
                        if (realmInstance != null) {
                            realmInstance.close();
                        }
                    }
                });
    }
}