package com.blinkseven.jrviewer.net.converter;

import com.blinkseven.jrviewer.db.model.AuthorizationModel;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.net.response.StatusResponse;
import com.blinkseven.jrviewer.net.response.CommentResponse;
import com.blinkseven.jrviewer.net.response.PostDetailResponse;
import com.blinkseven.jrviewer.net.response.PostsResponse;
import com.blinkseven.jrviewer.net.response.SearchResponse;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class JRViewerConverterFactory extends Converter.Factory {
    public static JRViewerConverterFactory create() {
        return new JRViewerConverterFactory();
    }

    private JRViewerConverterFactory() {
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        if (type == PostsResponse.class) {
            return PostsResponseConverter.INSTANCE;
        } else if (type == PostDetailResponse.class) {
            return PostDetailResponseConverter.INSTANCE;
        } else if (type == CommentResponse.class) {
            return CommentResponseConverter.INSTANCE;
        } else if (type == SearchResponse.class) {
            return SearchResponseConverter.INSTANCE;
        } else if (type == AuthorizationModel.class) {
            return AuthorizationModelConverter.INSTANCE;
        } else if (type == StatusResponse.class) {
            return StatusResponseConverter.INSTANCE;
        } else if (type == UserModel.class) {
            return UserModelConverter.INSTANCE;
        }
        return null;
    }
}