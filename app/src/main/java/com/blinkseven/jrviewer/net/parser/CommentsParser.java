package com.blinkseven.jrviewer.net.parser;

import android.support.annotation.NonNull;

import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.util.Utils;

import org.jsoup.nodes.Element;

public class CommentsParser {

    public CommentModel apply(@NonNull Element element) {
        CommentModel commentModel = new CommentModel();

//        commentModel.setPostId(element.attr("id").replace("comment_list_post_3202146", ""));
        String id = element.attr("id").replace("comment", "");
        commentModel.setId(id);

        if (element.parent().className().equals("comment_list")) {
            String parentId = element.parent().attr("id").replace("comment_list_comment_", "");
            commentModel.setParentId(parentId);
        }

        Element bottom = element.select(".comments_bottom").first();
        commentModel.setUserAvatar(bottom.select("img").attr("src"));
        commentModel.setUserName(bottom.select(".reply-link > a").first().text());
        commentModel.setUserUrn(bottom.select(".reply-link > a").first().attr("href"));

        long created = -1;
        if (bottom.select(".comment_date").size() != 0) {
            created = Utils.getDateTime(
                    bottom.select(".date").text(),
                    bottom.select(".time").text())
                    .getTime();
        }
        commentModel.setCreated(created);

        String rating = bottom.select(".comment_rating > span").text();
        commentModel.setRating(rating.isEmpty() ? 0f : Float.parseFloat(rating));

        Element imageBlock = element.select(".image").first();
        if (imageBlock != null) {
            commentModel.setImage(new AttachmentParser().apply(imageBlock));
        }

        commentModel.setText(getText(element.select(".txt").first()));

        return commentModel;
    }

    private String getText(Element content) {
        content.select(".image").remove();
        content.select(".comments_bottom").remove();
        return content.html();
    }
}