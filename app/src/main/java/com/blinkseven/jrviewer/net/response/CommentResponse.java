package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.model.ImageModel;

public class CommentResponse {
    private String text;
    private ImageModel image;

    public CommentResponse(String text, ImageModel image) {
        this.text = text;
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ImageModel getImage() {
        return image;
    }

    public void setImage(ImageModel image) {
        this.image = image;
    }
}