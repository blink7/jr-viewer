package com.blinkseven.jrviewer.net.parser;

import android.support.annotation.NonNull;
import android.util.Log;

import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.db.model.PollModel;
import com.blinkseven.jrviewer.db.model.PollOptionModel;
import com.blinkseven.jrviewer.db.model.TagModel;
import com.blinkseven.jrviewer.net.response.ResponsePostModel;
import com.blinkseven.jrviewer.util.Utils;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.RealmList;

public class PostParser {

    private static final String TAG = PostParser.class.getSimpleName();

    public ResponsePostModel apply(@NonNull Element element) {
        ResponsePostModel responsePost = new ResponsePostModel();

        responsePost.setId(element.id().replace("postContainer", ""));
        responsePost.setUserAvatar(element.select(".uhead_nick > img").attr("src")
                .replace(".reactor.cc", ".joyreactor.cc"));
        responsePost.setUserName(element.select(".uhead_nick > a").text());
        responsePost.setUserUrn(element.select(".uhead_nick > a").attr("href"));

        responsePost.setTags(getTags(element.select(".taglist").first()));

        List<ImageModel> imageModels = new ArrayList<>();
        Element postContent = element.select(".post_content").first();
        if (postContent != null) {
            AttachmentParser attachmentParser = new AttachmentParser();
            for (Element imageBlock : postContent.select(".image")) {
                ImageModel imageModel = attachmentParser.apply(imageBlock);
                if (imageModel != null) {
                    imageModels.add(imageModel);
                }
            }

            responsePost.setPoll(getPoll(postContent));
            responsePost.setText(getText(postContent));
        }
        responsePost.setAttachments(imageModels);

        responsePost.setCommentNum(Integer.parseInt(element.select(".commentnum")
                .text()
                .replace("Комментарии ", "")));
        responsePost.setCreated(Utils.getDateTime(
                element.select(".ufoot_first > span > span > span.date").text(),
                element.select(".ufoot_first > span > span > span.time").text())
                .getTime());

        String rating = element
                .select(".ufoot_first > span.post_rating > span")
                .text();
        responsePost.setRating(rating.isEmpty() ? 0f : Float.parseFloat(rating));

        return responsePost;
    }

    private List<TagModel> getTags(Element tagList) {
        List<TagModel> tags = new ArrayList<>();
        Elements elements = tagList.select("a");
        if (elements.isEmpty()) {
            return tags;
        }
        for (Element element : elements) {
            TagModel tag = new TagModel();
            tag.setName(element.text());
            tags.add(tag);
        }
        return tags;
    }

    private PollModel getPoll(Element content) {
        Elements poll = content.select(".post_poll_holder");
        if (poll.size() != 0) {
            PollModel pollModel = new PollModel();
            pollModel.setTitle(poll.select(".poll_quest").text());
            int size = poll.select("td[colspan=2]").size();
            RealmList<PollOptionModel> options = new RealmList<>();
            for (int i = 0; i < size; i++) {
                String pattern = "(?<=\\().+(?=%\\))";
                Matcher matcher = Pattern
                        .compile(pattern)
                        .matcher(poll.select("td[width=100]").get(i).text());
                if (matcher.find()) {
                    PollOptionModel option = new PollOptionModel(
                            poll.select("td[colspan=2]").get(i).text(),
                            Integer.parseInt(poll.select("td[width=100] > b").get(i).text()),
                            Float.parseFloat(matcher.group()));
                    options.add(option);
                }
            }
            pollModel.setOptions(options);
            return pollModel;
        }
        return null;
    }

    private String getText(Element content) {
        content.select(".image").remove();
        content.select(".mainheader").remove();
        content.select(".blog_results").remove();
        content.select(".post_poll_holder").remove();

        for (Element tag : content.select("*")) {
            if (!tag.hasText() && tag.parent() != null) {
                tag.remove();
            }
        }

        if (content.select("div").size() != 0) {
            return content.html().replaceAll("<h\\d>", "<b>").replaceAll("</h\\d>", "</b><p>");
        }
        return "";
    }
}