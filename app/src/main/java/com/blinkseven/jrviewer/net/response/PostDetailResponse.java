package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.CommentModel;

import java.util.List;

import io.realm.RealmObject;

public class PostDetailResponse {
    private ResponsePostModel postData;
    private List<CommentModel> comments;

    public void setPostData(ResponsePostModel postData) {
        this.postData = postData;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    public RealmObject getPost() {
        if (postData != null) {
            return postData.toPostModel(PostType.DETAIL);
        }
        return null;
    }
}