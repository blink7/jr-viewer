package com.blinkseven.jrviewer.net.parser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.blinkseven.jrviewer.db.model.ImageModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AttachmentParser {

    @Nullable
    public ImageModel apply(@NonNull Element element) {
        ImageModel gif = parseAsGif(element);
        if (gif != null) {
            return gif;
        } else {
            ImageModel image = parseAsImage(element);
            if (image != null) {
                return image;
            } else {
                ImageModel video = parseAsVideo(element);
                if (video != null) {
                    return video;
                }
            }
        }
        return null;
    }

    private ImageModel parseAsImage(Element content) {
        if (content.select("img").size() != 0) {
            ImageModel image = new ImageModel();
            image.setType(ImageModel.ImageType.IMG);
            image.setUri(content.select("img").attr("src")
                    .replace(".reactor.cc", ".joyreactor.cc"));
            if (content.select("a").size() != 0) {
                image.setFullUri(content.select("a").attr("href"));
            } else {
                image.setFullUri(image.getUri());
            }
            image.setWidth(Integer.parseInt(content.select("img").attr("width")));
            String height = content.select("img").attr("height");
            image.setHeight(Integer.parseInt(height.replace("%", "0")));
            return image;
        }
        return null;
    }

    private ImageModel parseAsGif(Element content) {
        if (content.select(".video_gif_holder").size() != 0) {
            ImageModel gif = new ImageModel();
            gif.setType(ImageModel.ImageType.GIF);
            gif.setFullUri(content.select(".video_gif_source").attr("href"));
            gif.setUri(content.select("img").attr("src"));
            if (content.select("video").size() != 0) {
                gif.setWidth(Integer.parseInt(content.select("video").attr("width")));
                gif.setHeight(Integer.parseInt(content.select("video").attr("height")));
            } else {
                gif.setWidth(Integer.parseInt(content.select("img").attr("width")));
                gif.setHeight(Integer.parseInt(content.select("img").attr("height")));
            }
            return gif;
        }
        return null;
    }

    private ImageModel parseAsVideo(Element content) {
        if (content.select("iframe").size() != 0) {
            ImageModel iframe = new ImageModel();
            iframe.setWidth(Integer.parseInt(content.select("iframe").attr("width")));
            iframe.setHeight(Integer.parseInt(content.select("iframe").attr("height")));
            iframe.setFullUri(content.select("iframe").attr("src"));

            if (content.select("iframe").hasClass("youtube-player")) {
                iframe.setType(ImageModel.ImageType.YouTube);
                Matcher matcher = Pattern
                        .compile("(?<=/embed/).+(?=\\?)")
                        .matcher(iframe.getFullUri());
                if (matcher.find()) {
                    iframe.setUri("http://img.youtube.com/vi/" + matcher.group() + "/0.jpg");
                }
            } else if (iframe.getFullUri().contains("coub.com")) {
                iframe.setType(ImageModel.ImageType.Coub);
                try {
                    String uri = new JSONObject(Jsoup
                            .connect("http://coub.com/api/oembed.json?url=" + iframe.getFullUri())
                            .ignoreContentType(true).execute().body()).getString("thumbnail_url");
                    iframe.setUri(uri);
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            } else if (iframe.getFullUri().contains("vimeo.com")) {
                iframe.setType(ImageModel.ImageType.vimeo);
                try {
                    Matcher matcher = Pattern
                            .compile("(?<=/video/).+(?=\\?)")
                            .matcher(iframe.getFullUri());
                    if (matcher.find()) {
                        String uri = new JSONArray(Jsoup
                                .connect("http://vimeo.com/api/v2/video/" + matcher.group() + ".json")
                                .ignoreContentType(true).execute().body())
                                .getJSONObject(0).getString("thumbnail_large");
                        iframe.setUri(uri);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            } else {
                return null;
            }

            return iframe;
        }
        return null;
    }
}