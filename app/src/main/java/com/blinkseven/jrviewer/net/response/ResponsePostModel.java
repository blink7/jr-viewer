package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.db.model.PollModel;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.db.model.PostDetailModel;
import com.blinkseven.jrviewer.db.model.PostFavoriteModel;
import com.blinkseven.jrviewer.db.model.PostFriendlineModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;
import com.blinkseven.jrviewer.db.model.TagModel;

import java.util.Collections;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ResponsePostModel {
    private String id;

    private String userAvatar;
    private String userName;
    private String userUrn;

    private List<TagModel> tags;

    private String text;
    private List<ImageModel> attachments;
    private PollModel poll;

    private int commentNum;
    private long created;
    private float rating;

    public RealmObject toPostModel(PostType type) {
        PostDataModel data = new PostDataModel();
        data.setId(id);
        data.setUserAvatar(userAvatar);
        data.setUserName(userName);
        data.setUserUrn(userUrn);
        Collections.sort(tags, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        data.setTags(new RealmList<>(tags.toArray(new TagModel[tags.size()])));
        data.setText(text);
        data.setContent(new RealmList<>(attachments.toArray(new ImageModel[attachments.size()])));
        data.setPoll(poll);
        data.setCommentNum(commentNum);
        data.setCreated(created);
        data.setRating(rating);

        switch (type) {
            case GOOD:
                PostGoodModel postGood = new PostGoodModel();
                postGood.setId(id);
                postGood.setData(data);
                return postGood;
            case ALL:
                PostAllModel postAll = new PostAllModel();
                postAll.setId(id);
                postAll.setData(data);
                return postAll;
            case BEST:
                PostBestModel postBest = new PostBestModel();
                postBest.setId(id);
                postBest.setData(data);
                return postBest;
            case ABYSS:
                PostAbyssModel postAbyss = new PostAbyssModel();
                postAbyss.setId(id);
                postAbyss.setData(data);
                return postAbyss;
            case DETAIL:
                PostDetailModel postDetail = new PostDetailModel();
                postDetail.setId(id);
                postDetail.setData(data);
                return postDetail;
            case TAG:
                PostTagModel postTag = new PostTagModel();
                postTag.setId(id);
                postTag.setData(data);
                return postTag;
            case USER:
                PostUserModel postUser = new PostUserModel();
                postUser.setId(id);
                postUser.setData(data);
                return postUser;
            case FAVORITE:
                PostFavoriteModel postFavorite = new PostFavoriteModel();
                postFavorite.setId(id);
                postFavorite.setData(data);
                return postFavorite;
            case FRIENDLINE:
                PostFriendlineModel postFriendline = new PostFriendlineModel();
                postFriendline.setId(id);
                postFriendline.setData(data);
                return postFriendline;
            default:
                return null;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserUrn() {
        return userUrn;
    }

    public void setUserUrn(String userUrn) {
        this.userUrn = userUrn;
    }

    public List<TagModel> getTags() {
        return tags;
    }

    public void setTags(List<TagModel> tags) {
        this.tags = tags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ImageModel> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<ImageModel> attachments) {
        this.attachments = attachments;
    }

    public PollModel getPoll() {
        return poll;
    }

    public void setPoll(PollModel poll) {
        this.poll = poll;
    }

    public int getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(int commentNum) {
        this.commentNum = commentNum;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}