package com.blinkseven.jrviewer.net.response;

import com.blinkseven.jrviewer.db.model.CommentModel;

import java.util.List;

public class CommentsResponse {
    private List<CommentModel> comments;

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public boolean isEmpty() {
        return comments == null;
    }
}