package com.blinkseven.jrviewer.net.converter;

import android.support.annotation.NonNull;

import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.net.parser.AttachmentParser;
import com.blinkseven.jrviewer.net.response.CommentResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class CommentResponseConverter implements Converter<ResponseBody, CommentResponse> {
    static final CommentResponseConverter INSTANCE = new CommentResponseConverter();

    @Override
    public CommentResponse convert(@NonNull ResponseBody value) throws IOException {
        Document doc = Jsoup.parse(value.string());

        AttachmentParser attachmentParser = new AttachmentParser();
        ImageModel imageModel = null;
        if (doc.select(".image").size() != 0) {
            imageModel = attachmentParser.apply(doc.select(".image").first());
        }

        doc.select(".image").remove();
        String text = doc.html();

        return new CommentResponse(text, imageModel);
    }
}