package com.blinkseven.jrviewer;

import android.app.Application;

import com.blinkseven.jrviewer.db.DatabaseHelper;
import com.blinkseven.jrviewer.net.JRNetService;
import com.blinkseven.jrviewer.net.converter.JRViewerConverterFactory;
import com.crashlytics.android.Crashlytics;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.ImageLoader;
import com.github.piasy.biv.loader.fresco.FrescoImageLoader;

import org.androidannotations.annotations.EApplication;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@EApplication
public class JRViewerApp extends Application {

    private static OkHttpClient client;

    private static final int READ_TIMEOUT = 30;
    private static final int WRITE_TIMEOUT = 120;
    private static final int CONNECT_TIMEOUT = 10;

    private static JRNetService apiService;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        DatabaseHelper.configure();
        initClient();
        initFresco();
//        initFabric();
    }

    public static JRNetService getApiService() {
        if (apiService == null) {
            apiService = getRetrofit();
        }

        return apiService;
    }

    private void initClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        Interceptor headerInterceptor = chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder()
                    .addHeader("Referer", JRViewerApp_.getInstance().getString(R.string.api_server))
                    .method(original.method(), original.body());
            Request request = requestBuilder.build();
            return chain.proceed(request);
        };

        ClearableCookieJar cookieJar = new PersistentCookieJar(
                new SetCookieCache(),
                new SharedPrefsCookiePersistor(this));

        client = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(headerInterceptor)
                .addNetworkInterceptor(logging)
                .cookieJar(cookieJar)
                .build();
    }

    private void initFresco() {
        DiskCacheConfig mainCacheConfog = DiskCacheConfig.newBuilder(this)
                .setMaxCacheSize(128 * ByteConstants.MB)
                .build();
        DiskCacheConfig smallCacheConfog = DiskCacheConfig.newBuilder(this)
                .setMaxCacheSize(18 * ByteConstants.MB)
                .build();
        ImagePipelineConfig imagePipelineConfig = OkHttpImagePipelineConfigFactory
                .newBuilder(this, client)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
//                .setDownsampleEnabled(true)
                .setMainDiskCacheConfig(mainCacheConfog)
                .setSmallImageDiskCacheConfig(smallCacheConfog)
                .build();
        Fresco.initialize(this, imagePipelineConfig);

        ImageLoader imageLoader
                = FrescoImageLoader.with(this, imagePipelineConfig);
        BigImageViewer.initialize(imageLoader);
    }

    private void initFabric() {
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
    }

    private static JRNetService getRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory
                        .createWithScheduler(Schedulers.newThread()))
                .baseUrl(JRViewerApp_.getInstance().getString(R.string.api_server))
                .addConverterFactory(JRViewerConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(JRNetService.class);
    }
}