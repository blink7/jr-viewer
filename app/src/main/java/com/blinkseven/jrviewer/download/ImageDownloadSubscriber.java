package com.blinkseven.jrviewer.download;

import android.content.Context;

import com.blinkseven.jrviewer.util.FileHelper;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.memory.PooledByteBufferInputStream;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.BaseDataSubscriber;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.DefaultExecutorSupplier;
import com.facebook.imagepipeline.request.ImageRequest;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.Observable;

public class ImageDownloadSubscriber {

    public static synchronized Observable<File> getImageFromCache(Context context, String uri) {
        return Observable.create(emitter -> Fresco.getImagePipeline()
                .fetchEncodedImage(ImageRequest.fromUri(uri), true)
                .subscribe(new BaseDataSubscriber<CloseableReference<PooledByteBuffer>>() {
                    @Override
                    protected void onNewResultImpl(DataSource<CloseableReference<PooledByteBuffer>> dataSource) {
                        PooledByteBufferInputStream in = null;
                        FileOutputStream out = null;
                        try {
                            if (!dataSource.isFinished() || dataSource.getResult() == null) {
                                return;
                            }

                            File tempFile = FileHelper.createTempImageFile(context, uri);
                            in = new PooledByteBufferInputStream(dataSource.getResult().get());
                            out = new FileOutputStream(tempFile);
                            IOUtils.copy(in, out);
                            emitter.onNext(tempFile);
                        } catch (IOException e) {
                            emitter.onError(e);
                        } finally {
                            IOUtils.closeQuietly(in, out);
                            if (dataSource.isFinished()) {
                                dataSource.close();
                            }
                        }
                    }

                    @Override
                    protected void onFailureImpl(DataSource<CloseableReference<PooledByteBuffer>> dataSource) {
                        emitter.onError(new RuntimeException("onFailureImpl"));
                    }
                }, new DefaultExecutorSupplier(Runtime.getRuntime()
                        .availableProcessors())
                        .forBackgroundTasks()));
    }
}