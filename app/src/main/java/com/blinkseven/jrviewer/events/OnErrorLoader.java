package com.blinkseven.jrviewer.events;

public class OnErrorLoader {

    private Throwable throwable;

    public OnErrorLoader(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}