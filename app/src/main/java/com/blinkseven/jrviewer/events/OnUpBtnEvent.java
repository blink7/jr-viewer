package com.blinkseven.jrviewer.events;

public class OnUpBtnEvent {

    private boolean show;

    public OnUpBtnEvent(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }
}