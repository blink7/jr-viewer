package com.blinkseven.jrviewer.events;

public class OnErrorPostDetail {
    private Throwable error;

    public OnErrorPostDetail(Throwable error) {
        this.error = error;
    }

    public Throwable getError() {
        return error;
    }
}