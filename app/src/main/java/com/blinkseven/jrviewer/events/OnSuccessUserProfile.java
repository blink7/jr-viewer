package com.blinkseven.jrviewer.events;

import com.blinkseven.jrviewer.db.model.UserModel;

public class OnSuccessUserProfile {

    private UserModel user;

    public OnSuccessUserProfile(UserModel user) {
        this.user = user;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}