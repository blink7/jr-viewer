package com.blinkseven.jrviewer.events;

public class PostsEvent {
    private final int page;
    private final int pagesCount;
    private String redirectionUrl;

    public PostsEvent(int page, int pagesCount) {
        this.page = page;
        this.pagesCount = pagesCount;
    }

    public int getPage() {
        return page;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public String getRedirectionUrl() {
        return redirectionUrl;
    }

    public void setRedirectionUrl(String redirectionUrl) {
        this.redirectionUrl = redirectionUrl;
    }
}