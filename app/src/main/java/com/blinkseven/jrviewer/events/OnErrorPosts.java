package com.blinkseven.jrviewer.events;

public class OnErrorPosts {
    private String msg;

    public OnErrorPosts(Throwable throwable) {
        msg = throwable.getMessage();
        throwable.printStackTrace();
    }

    public String getMessage() {
        return msg;
    }
}