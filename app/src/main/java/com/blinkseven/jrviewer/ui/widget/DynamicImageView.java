package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.view.SimpleDraweeView;

public class DynamicImageView extends SimpleDraweeView {

    private float hRatio = 0;
    private float wRatio = 0;

    public DynamicImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicImageView(Context context) {
        super(context);
    }

    public void setHeightRatio(float ratio) {
        hRatio = ratio;
    }

    public void setWidthRatio(float ratio) {
        wRatio = ratio;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (hRatio != 0) {
            int width = getMeasuredWidth();
            int height = (int) (hRatio * width);
            setMeasuredDimension(width, height);
        } else if (wRatio != 0) {
            int height = getMeasuredHeight();
            int width = (int) (wRatio * height);
            setMeasuredDimension(width, height);
        }
    }
}