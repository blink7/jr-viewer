package com.blinkseven.jrviewer.ui.widget.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

import com.github.piasy.biv.indicator.ProgressIndicator;
import com.github.piasy.biv.view.BigImageView;

public class LoadingProgressBarView extends View implements ProgressIndicator {

    private LoadingProgressBarDrawable progressBar;

    public LoadingProgressBarView(Context context, LoadingProgressBarDrawable progressBar) {
        super(context);
        this.progressBar = progressBar;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        progressBar.setBounds(0, 0, getWidth(), getHeight());
        progressBar.draw(canvas);
    }

    @Override
    public View getView(BigImageView parent) {
        return this;
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onProgress(int progress) {
        progressBar.onLevelChange(progress * 100);
        invalidate();
    }

    @Override
    public void onFinish() {
    }
}