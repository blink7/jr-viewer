package com.blinkseven.jrviewer.ui;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.support.v7.widget.PopupMenu;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.events.OnSuccessUserProfile;
import com.blinkseven.jrviewer.net.UserSubscriber;
import com.blinkseven.jrviewer.ui.widget.progress.UserRatingBar;
import com.blinkseven.jrviewer.ui.widget.progress.CircleProgressBar;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.ImageDecodeOptionsBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.parceler.Parcels;

import io.reactivex.disposables.Disposable;

import static com.blinkseven.jrviewer.util.Utils.disposeQuietly;
import static com.blinkseven.jrviewer.util.Utils.showProgress;

public class UserBottomSheet
        extends AppCompatDialogFragment
        implements PopupMenu.OnMenuItemClickListener {

    private static final String TAG = UserBottomSheet.class.getSimpleName();

    private TextView nameView;
    private ImageButton moreButton;
    private UserRatingBar starsBar;
    private SimpleDraweeView avatarView;
    private CircleProgressBar progressBar;
    private TextView postsNumView;
    private TextView goodPostsNumView;
    private TextView bestPostsNumView;
    private TextView commentsView;
    private TextView sinceView;
    private TextView lastVisitView;
    private TextView consecutiveDaysView;

    private View userDetailsView;
    private View loadingProgressView;

    private UserModel user;

    private Disposable subscription;
    private final EventBus bus = EventBus.getDefault();

    private static final String USER_KEY = "user";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(USER_KEY, Parcels.wrap(user));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            user = Parcels.unwrap(savedInstanceState.getParcelable(USER_KEY));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_sheet, container, false);
        nameView = v.findViewById(R.id.name);
        moreButton = v.findViewById(R.id.more);
        moreButton.setOnClickListener(this::showPopup);
        starsBar = v.findViewById(R.id.user_stars);
        avatarView = v.findViewById(R.id.avatar);
        progressBar = v.findViewById(R.id.user_progress);
        postsNumView = v.findViewById(R.id.posts_num);
        goodPostsNumView = v.findViewById(R.id.good_posts_num);
        bestPostsNumView = v.findViewById(R.id.best_posts_num);
        commentsView = v.findViewById(R.id.comments_num);
        sinceView = v.findViewById(R.id.since);
        lastVisitView = v.findViewById(R.id.last_visit);
        consecutiveDaysView = v.findViewById(R.id.consecutive_days);

        userDetailsView = v.findViewById(R.id.user_details);
        loadingProgressView = v.findViewById(R.id.loading_user_progress);

        initData();

        return v;
    }

    private void initData() {
        nameView.setText(user.getUserName());
        setupAvatarView(user.getUserAvatar());
        if (user.isComplete()) {
            postsNumView.setText(user.getTotalPostsNumber());
            goodPostsNumView.setText(user.getGoodPostsNumber());
            bestPostsNumView.setText(user.getBestPostsNumber());
            commentsView.setText(user.getCommentsNumber());
            sinceView.setText(user.getSince());
            lastVisitView.setText(user.getLastVisit());
            consecutiveDaysView.setText(user.getConsecutiveDays());
        } else {
            showProgress(userDetailsView, loadingProgressView, true);
            subscription = UserSubscriber.loadUserById(user.getUserName());
        }
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog =
                new BaseBottomSheetDialog(getActivity(), R.style.UserBottomSheetTheme);

        /*FrameLayout bottomSheet = ((BottomSheetDialog) dialog)
                .findViewById(android.support.design.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(Utils.dpToPx(112));*/

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
        if (user != null) {
            progressBar.setProgressWithAnimation(user.getProgress());
            starsBar.setScoreWithAnimation(user.getStars());
        }
    }

    private void setupAvatarView(String avatarUrn) {
        ImageDecodeOptions imageDecodeOptions = new ImageDecodeOptionsBuilder()
                .setForceStaticImage(true)
                .build();
        ImageRequest avatarRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(avatarUrn))
                .setImageDecodeOptions(imageDecodeOptions)
                .setCacheChoice(ImageRequest.CacheChoice.SMALL)
                .build();
        avatarView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(avatarView.getController())
                        .setImageRequest(avatarRequest)
                        .build());
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getContext(), v, Gravity.NO_GRAVITY,
                R.attr.actionOverflowMenuStyle, 0);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.user_sheet_actions);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.user_posts:
                DataActivity_
                        .intent(getContext())
                        .type(PostType.USER)
                        .value(user.getUserName())
                        .start();
                break;
            case R.id.favorite:
                DataActivity_
                        .intent(getContext())
                        .type(PostType.FAVORITE)
                        .value(user.getUserName())
                        .start();
                break;
            case R.id.friendline:
                DataActivity_
                        .intent(getContext())
                        .type(PostType.FRIENDLINE)
                        .value(user.getUserName())
                        .start();
                break;
        }
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserSuccessEvent(final OnSuccessUserProfile event) {
        user = event.getUser();
        showProgress(userDetailsView, loadingProgressView, false);
        progressBar.setProgressWithAnimation(user.getProgress());
        starsBar.setScoreWithAnimation(user.getStars());
        initData();
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        disposeQuietly(subscription);
        super.onDestroy();
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    static class BaseBottomSheetDialog extends BottomSheetDialog {

        public BaseBottomSheetDialog(@NonNull Context context, int theme) {
            super(context, theme);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            int width = getContext().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_width);
            getWindow().setLayout(width > 0 ? width : ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }
}