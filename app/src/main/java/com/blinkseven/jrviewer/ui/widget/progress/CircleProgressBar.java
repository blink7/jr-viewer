package com.blinkseven.jrviewer.ui.widget.progress;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;

import com.blinkseven.jrviewer.R;

public class CircleProgressBar extends View {

    private int startAngel = 270;
    private int sweepAngle = 360;
    private int progressColor = 0xFFFFFFFF;
    private int backgroundColor = 0xFF000000;
    private int progressWidth = 10;
    private int backgroundWidth = 6;
    private float max = 100;
    private float progress = 0;

    private float progressSweep = 0;

    private Paint progressPaint;
    private Paint backgroundPaint;

    private RectF progressBounds = new RectF();

    public CircleProgressBar(Context context) {
        this(context, null, 0);
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CircleProgressBar,
                    defStyleAttr,
                    0);

            startAngel = a.getInteger(R.styleable.CircleProgressBar_startAngle, startAngel);
            sweepAngle = a.getInteger(R.styleable.CircleProgressBar_sweepAngle, sweepAngle);
            progressColor = a.getColor(R.styleable.CircleProgressBar_progressColor, progressColor);
            backgroundColor
                    = a.getColor(R.styleable.CircleProgressBar_backgroundColor, backgroundColor);
            progressWidth = (int) a.getDimension(
                    R.styleable.CircleProgressBar_progressWidth, progressWidth);
            backgroundWidth = (int) a.getDimension(
                    R.styleable.CircleProgressBar_backgroundWidth, backgroundWidth);
            max = a.getFloat(R.styleable.CircleProgressBar_max, max);
            progress = a.getFloat(R.styleable.CircleProgressBar_progress, progress);
        }

        if (progress > max) {
            progress = max;
        }
        if (progress < 0) {
            progress = 0;
        }
        if (sweepAngle > 360) {
            sweepAngle = 360;
        }
        if (sweepAngle < 0) {
            sweepAngle = 0;
        }

        progressSweep = progress / max * sweepAngle;

        if (startAngel > 360) {
            startAngel = 360;
        }
        if (startAngel < 0) {
            startAngel = 0;
        }

        setupProgressPaint();
        setupBackgroundPaint();
    }

    private void setupProgressPaint() {
        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(progressWidth);
    }

    private void setupBackgroundPaint() {
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStrokeWidth(backgroundWidth);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(),
                heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(),
                widthMeasureSpec);
        final int min = Math.min(width, height);
        int arcDiameter = min - progressWidth;
        int arcRadius = arcDiameter / 2;
        float top = height / 2 - arcRadius;
        float left = width / 2 - arcRadius;
        progressBounds.set(left, top, left + arcDiameter, top + arcDiameter);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int arcStart = startAngel + -90;
        canvas.drawArc(progressBounds, arcStart, sweepAngle, false, backgroundPaint);
        canvas.drawArc(progressBounds, arcStart, progressSweep, false, progressPaint);
    }

    public void onProgress(float progress) {
        if (progress == -1) {
            return;
        }
        if (progress > max) {
            progress = max;
        }
        if (progress < 0) {
            progress = 0;
        }
        this.progress = progress;
        progressSweep = progress / max * sweepAngle;
        invalidate();
    }

    public int getStartAngel() {
        return startAngel;
    }

    public void setStartAngel(int startAngel) {
        this.startAngel = startAngel;
    }

    public int getSweepAngle() {
        return sweepAngle;
    }

    public void setSweepAngle(int sweepAngle) {
        this.sweepAngle = sweepAngle;
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        invalidate();
    }

    public int getBackColor() {
        return backgroundColor;
    }

    public void setBackColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        invalidate();
    }

    public int getProgressWidth() {
        return progressWidth;
    }

    public void setProgressWidth(int progressWidth) {
        this.progressWidth = progressWidth;
        progressPaint.setStrokeWidth(progressWidth);
    }

    public int getBackgroundWidth() {
        return backgroundWidth;
    }

    public void setBackgroundWidth(int backgroundWidth) {
        this.backgroundWidth = backgroundWidth;
        backgroundPaint.setStrokeWidth(backgroundWidth);
    }

    public float getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        onProgress(progress);
    }

    public void setProgressWithAnimation(float progress) {
        this.progress = progress;
        ObjectAnimator progressAnimator
                = ObjectAnimator.ofFloat(this, "progress", 0, progress).setDuration(1000);
        progressAnimator.setInterpolator(new FastOutSlowInInterpolator());
        progressAnimator.start();
    }
}