package com.blinkseven.jrviewer.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.util.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.ImageDecodeOptionsBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.drawer_layout)
    DrawerLayout drawer;

    SimpleDraweeView avatarView;
    TextView nameView;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;
    @ViewById(R.id.navigation_bottom)
    BottomNavigationView bottomNavigation;
    @ViewById(R.id.up_btn)
    FloatingActionButton upButton;

    @InstanceState
    boolean isNewInstance = true;

    private FragmentManager fm = getSupportFragmentManager();

    private static final int NO_ACTION = 0;
    private AtomicInteger drawerForAction = new AtomicInteger(NO_ACTION);
    private AtomicInteger currentFragmentIndex = new AtomicInteger(0);

    @InstanceState
    int activeTabPosition;

    private PostsFragment[] postsFragments = new PostsFragment[]{
            PostsFragment.getInstance(PostType.GOOD),
            PostsFragment.getInstance(PostType.ALL),
            PostsFragment.getInstance(PostType.BEST),
            PostsFragment.getInstance(PostType.ABYSS)
    };

    private UserModel user;

    private Realm realmInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realmInstance = Realm.getDefaultInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = realmInstance
                .where(UserModel.class)
                .equalTo("activeUser", true)
                .findFirst();
        initUserInfo();
    }

    @AfterViews
    public void init() {
        setSupportActionBar(toolbar);
        setupDrawer();
        setupBottomNavigation();
        if (isNewInstance) {
            isNewInstance = false;
            setTitle(R.string.fragment_good);
            replaceFragment(postsFragments[0]);
        }
        upButton.setOnClickListener(v -> upList());
    }

    private void setupDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                switch (drawerForAction.get()) {
                    case R.id.nav_profile:
                        showUserSheet(fm, user);
                        break;
                    case R.id.nav_settings:
                        //TODO
                        break;
                }
                drawerForAction.set(NO_ACTION);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nav_profile:
                    //TODO
                    drawerForAction.set(R.id.nav_profile);
                    drawer.closeDrawer(Gravity.START);
                    break;
                case R.id.nav_settings:
                    //TODO
            }
            drawer.closeDrawer(GravityCompat.START);
            return true;
        });

        avatarView = navigationView.getHeaderView(0).findViewById(R.id.avatar);
        nameView = navigationView.getHeaderView(0).findViewById(R.id.name);
    }

    private void initUserInfo() {
        if (user != null) {
            nameView.setOnClickListener(null);
            nameView.setText(user.getUserName());

            ImageDecodeOptions imageDecodeOptions = new ImageDecodeOptionsBuilder()
                    .setForceStaticImage(true)
                    .build();
            ImageRequest avatarRequest = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(user.getUserAvatar()))
                    .setImageDecodeOptions(imageDecodeOptions)
                    .setCacheChoice(ImageRequest.CacheChoice.SMALL)
                    .build();
            avatarView.setController(
                    Fresco.newDraweeControllerBuilder()
                            .setOldController(avatarView.getController())
                            .setImageRequest(avatarRequest)
                            .build());
        } else {
            nameView.setOnClickListener(v -> AuthorizationActivity_
                    .intent(this).fromDrawer(true).startForResult(AuthorizationActivity.CODE));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            realmInstance.addChangeListener(realm -> {
                user = realm.where(UserModel.class).equalTo("activeUser", true).findFirst();
                initUserInfo();
            });
        }
    }

    private void setupBottomNavigation() {
        disableShiftMode(bottomNavigation);
        if (!bottomNavigation.getMenu().getItem(activeTabPosition).isChecked()) {
            bottomNavigation.getMenu().getItem(0).setChecked(false);
            bottomNavigation.getMenu().getItem(activeTabPosition).setChecked(true);
        }
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_good:
                    replaceFragment(postsFragments[0]);
                    setTitle(R.string.fragment_good);
                    currentFragmentIndex.set(0);
                    break;
                case R.id.navigation_all:
                    replaceFragment(postsFragments[1]);
                    currentFragmentIndex.set(1);
                    setTitle(R.string.fragment_all);
                    break;
                case R.id.navigation_best:
                    replaceFragment(postsFragments[2]);
                    currentFragmentIndex.set(2);
                    setTitle(R.string.fragment_best);
                    break;
                case R.id.navigation_new:
                    replaceFragment(postsFragments[3]);
                    currentFragmentIndex.set(3);
                    setTitle(R.string.fragment_new);
                    break;
            }
            return true;
        });
    }

    private void replaceFragment(Fragment fragment) {
        showUpButton(false);

        fm.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(i);
                itemView.setShiftingMode(false);
                // Set once again checked value, so view will be updated
                itemView.setChecked(itemView.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e(TAG, "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Unable to change value of shift mode", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        // Set the same padding of DropDownView
        // Get AutoCompleteTextView from SearchView
        final AutoCompleteTextView searchEditText = searchView.findViewById(R.id.search_src_text);
        final View dropDownAnchor = searchView.findViewById(searchEditText.getDropDownAnchor());
        if (dropDownAnchor != null) {
            dropDownAnchor.addOnLayoutChangeListener((v, left, top, right, bottom,
                                                      oldLeft, oldTop, oldRight, oldBottom) -> {
                // Calculate width of DropdownView
                int point[] = new int[2];
                dropDownAnchor.getLocationOnScreen(point);
                // x coordinate of DropDownView
                int dropDownPadding = point[0] + searchEditText.getDropDownHorizontalOffset();
                // Set DropDownView width
                searchEditText.setDropDownWidth(Utils.getDisplayWidth() - dropDownPadding * 2);
            });
        }
        return true;
    }

    @Override
    public void showUpButton(boolean show) {
        if (show) {
            upButton.show();
        } else {
            upButton.hide();
        }
    }

    public void upList() {
        postsFragments[currentFragmentIndex.get()].upList();
    }

    @Override
    protected void onDestroy() {
        realmInstance.close();
        super.onDestroy();
    }
}