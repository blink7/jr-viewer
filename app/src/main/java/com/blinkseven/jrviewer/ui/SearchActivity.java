package com.blinkseven.jrviewer.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.dao.SuggestionDao;

import org.androidannotations.annotations.EActivity;

@EActivity
public class SearchActivity extends AppCompatActivity {

    private String query;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // Handle the normal search query case
            query = intent.getStringExtra(SearchManager.QUERY);
            SuggestionDao.addSuggestion(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            Uri data = intent.getData();
            query = data.getLastPathSegment();
        }

        DataActivity_
                .intent(this)
                .type(PostType.TAG)
                .value(query)
                .start();
        finish();
    }
}