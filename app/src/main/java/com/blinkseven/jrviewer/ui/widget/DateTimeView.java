package com.blinkseven.jrviewer.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.blinkseven.jrviewer.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * A {@link android.widget.TextView} which is intended to display the date of the post
 * more convenient and shorter. It is inherited from {@link AppCompatTextView} to support
 * compatible features on older version of the platform.
 */
public class DateTimeView extends AppCompatTextView {
    private boolean fullMode;
    private Date date;

    public DateTimeView(Context context) {
        super(context);
    }

    public DateTimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DateTimeView,
                0, 0);

        try {
            fullMode = a.getBoolean(R.styleable.DateTimeView_fullMode, false);
            setDateTime(
                    a.getString(R.styleable.DateTimeView_sDate),
                    a.getString(R.styleable.DateTimeView_sTime));
        } finally {
            a.recycle();
        }
    }

    public DateTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Set date and time of the post to show it more convenient.
     *
     * @param sDate Date of the post in 'dd.MMM.yyyy' format
     * @param sTime Time of the post in 'HH:mm' format
     */
    public void setDateTime(String sDate, String sTime) {
        if (sDate == null || sTime == null) {
            return;
        }

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MMM.yyyy HH:mm");
        try {
            date = formatter.parse(sDate + " " + sTime);
            formatDateTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param millis Full date of the post in milliseconds
     */
    public void setDateTime(long millis) {
        date = new Date(millis);
        formatDateTime();
    }

    private void formatDateTime() {
        Date currDate = new Date();
        long duration = currDate.getTime() - date.getTime();
        String pattern = fullMode ? "%d %s" : "%d%s";
        if (duration < TimeUnit.MINUTES.toMillis(1)) {
            setText(fullMode ? R.string.second_long : R.string.second_short,
                    duration / TimeUnit.SECONDS.toMillis(1));
        } else if (duration < TimeUnit.HOURS.toMillis(1)) {
            setText(fullMode ? R.string.minute_long : R.string.minute_short,
                    duration / TimeUnit.MINUTES.toMillis(1));
        } else if (duration < TimeUnit.DAYS.toMillis(1)) {
            setText(fullMode ? R.string.hour_long : R.string.hour_short,
                    duration / TimeUnit.HOURS.toMillis(1));
        } else if (duration < TimeUnit.DAYS.toMillis(2)) {
            setText(fullMode ? R.string.yesterday_long : R.string.day_short);
        } else if (duration < TimeUnit.DAYS.toMillis(3)) {
            setText(fullMode ? R.string.two_days_long : R.string.two_four_day_short,
                    duration / TimeUnit.DAYS.toMillis(1));
        } else if (duration < TimeUnit.DAYS.toMillis(5)) {
            setText(fullMode ? R.string.day_long : R.string.two_four_day_short,
                    duration / TimeUnit.DAYS.toMillis(1));
        }  else if (duration < TimeUnit.DAYS.toMillis(7)) {
            setText(fullMode ? R.string.day_long : R.string.five_seven_day_short,
                    duration / TimeUnit.DAYS.toMillis(1));
        } else if (duration < TimeUnit.DAYS.toMillis(365)) {
            if (fullMode) {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat formatter = new SimpleDateFormat("MMMM d");
                setText(formatter.format(date));
            } else {
                setText(R.string.week_short, duration / TimeUnit.DAYS.toMillis(7));
            }
        } else {
            if (fullMode) {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat formatter = new SimpleDateFormat("MMMM d, yyyy");
                setText(formatter.format(date));
            } else {
                setText(R.string.week_short, duration / TimeUnit.DAYS.toMillis(7));
            }
        }
    }

    /**
     * Utility method.
     *
     * @param resId Time description
     * @param time The post time
     */
    private void setText(int resId, long time) {
        setText(getContext().getString(resId, time));
    }

    /**
     * @return Returns {@code true} if the {@link DateTimeView} is in expanded mode
     */
    public boolean isFullMode() {
        return fullMode;
    }

    /**
     * Set the display of the post time more concisely for common posts
     * and expanded for detailed posts.
     *
     * @param fullMode The param is {@code false} for concisely mode and {@code true} otherwise
     */
    public void setFullMode(boolean fullMode) {
        this.fullMode = fullMode;
        invalidate();
        requestLayout();
    }
}
