package com.blinkseven.jrviewer.ui.widget.progress;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextPaint;

public class LoadingProgressBarDrawable extends Drawable {

    private int barWidth = 10;
    private int fontSize = 50;
    private int barRadius = 100;

    private float currentProgress = 0;

    private Paint progressPaint;
    private Paint backgroundPaint;
    private Paint fontPaint;

    private RectF progressBounds = new RectF();

    private int color = 0x800080FF;
    private int backgroundColor = 0xFFEEEEEE;

    public LoadingProgressBarDrawable() {
        init();
    }

    private void init() {
        setupProgressPaint();
        setupBackgroundPaint();
        setupTextPaint();
    }

    private void setupProgressPaint() {
        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setColor(color);
        progressPaint.setStrokeWidth(barWidth);
    }

    private void setupBackgroundPaint() {
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStrokeWidth(barWidth - 4);
    }

    private void setupTextPaint() {
        fontPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        fontPaint.setStyle(Paint.Style.STROKE);
        fontPaint.setColor(color);
        fontPaint.setTextSize(fontSize);
    }

    /** Sets the progress bar width. */
    public void setBarWidth(int barWidth) {
        if (this.barWidth != barWidth) {
            this.barWidth = barWidth;
            invalidateSelf();
        }
    }

    /** Gets the progress bar width. */
    public int getBarWidth() {
        return barWidth;
    }

    /** Sets the progress bar radius. */
    public void setBarRadius(int barRadius) {
        if (this.barRadius != barRadius) {
            this.barRadius = barRadius;
            invalidateSelf();
        }
    }

    /** Gets the progress bar radius. */
    public int getBarRadius() {
        return barRadius;
    }

    /** Sets the progress text size. */
    public void setFontSize(int fontSize) {
        if (this.fontSize != fontSize) {
            this.fontSize = fontSize;
            fontPaint.setTextSize(fontSize);
            invalidateSelf();
        }
    }

    /** Gets the progress text size. */
    public int getFontSize() {
        return fontSize;
    }

    /** Sets the progress bar color. */
    public void setColor(int color) {
        if (this.color != color) {
            this.color = color;
            invalidateSelf();
        }
    }

    /** Gets the progress bar color. */
    public int getColor() {
        return color;
    }

    /** Sets the progress bar background color. */
    public void setBackgroundColor(int backgroundColor) {
        if (this.backgroundColor != backgroundColor) {
            this.backgroundColor = backgroundColor;
            invalidateSelf();
        }
    }

    /** Gets the progress bar background color. */
    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    protected boolean onLevelChange(int level) {
        currentProgress = level;
        invalidateSelf();
        return true;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        drawBackgroundArc(canvas);
        drawArcForCurrentProgress(canvas);
        drawText(canvas);
    }

    private void drawBackgroundArc(Canvas canvas) {
        updateProgressBounds();
        canvas.drawArc(progressBounds, -90f, 360, false, backgroundPaint);
    }

    private void drawArcForCurrentProgress(Canvas canvas) {
        updateProgressBounds();
        canvas.drawArc(progressBounds, -90f, 360 * currentProgress / 10000, false, progressPaint);
    }

    private void drawText(Canvas canvas) {
        String progress = (int) (currentProgress / 100) + "%";
        float textWidth = fontPaint.measureText(progress);
        float cX = (float) getBounds().centerX();
        float cY = (float) getBounds().centerY();
        canvas.drawText(
                progress,
                cX - textWidth / 2,
                cY - (fontPaint.ascent() + fontPaint.descent()) / 2,
                fontPaint);
    }

    private void updateProgressBounds() {
        float cX = (float) getBounds().centerX();
        float cY = (float) getBounds().centerY();
        progressBounds.set(cX - barRadius, cY - barRadius, cX + barRadius, cY + barRadius);
    }

    @Override
    public void setAlpha(@IntRange(from = 0, to = 255) int i) {
        progressPaint.setAlpha(i);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        progressPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        int color = progressPaint.getColor();
        int colorAlpha = color >>> 24;
        if (colorAlpha == 255) {
            return PixelFormat.OPAQUE;
        } else if (colorAlpha == 0) {
            return PixelFormat.TRANSPARENT;
        } else {
            return PixelFormat.TRANSLUCENT;
        }
    }
}