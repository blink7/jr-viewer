package com.blinkseven.jrviewer.ui;

import android.Manifest;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaScannerConnection;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.adapter.MediaPagerAdapter;
import com.blinkseven.jrviewer.download.ImageDownloadSubscriber;
import com.blinkseven.jrviewer.util.FileHelper;
import com.blinkseven.jrviewer.util.ZoomOutPageTransformer;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.blinkseven.jrviewer.util.Utils.disposeQuietly;

@Fullscreen
@EActivity(R.layout.activity_media)
public class MediaActivity
        extends AppCompatActivity
        implements MediaPagerAdapter.OnClickListener {

    public static final String TAG = MediaActivity.class.getSimpleName();

    @ViewById(R.id.media_pager)
    ViewPager mediaPager;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.container)
    RelativeLayout container;

    @Extra
    String[] uris;

    @Extra
    int position;

    private MediaPagerAdapter adapter;

    private Disposable permissionRequest;

    @AfterViews
    public void init() {
        setupPager();
        setupToolbar();
    }

    private void setupPager() {
        adapter = new MediaPagerAdapter(this, uris);
        mediaPager.setAdapter(adapter);
        mediaPager.setCurrentItem(position);
        mediaPager.setPageTransformer(true, new ZoomOutPageTransformer());
        adapter.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            // Add back arrow to toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.media_activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.download_image:
                downloadImage(uris[mediaPager.getCurrentItem()]);
                return true;
            case R.id.share_image:
                shareImage(uris[mediaPager.getCurrentItem()]);
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick() {
        TransitionDrawable transition = (TransitionDrawable) container.getBackground();
        if (getSupportActionBar().isShowing()) {
            getSupportActionBar().hide();
            transition.startTransition(300);
        } else {
            getSupportActionBar().show();
            transition.reverseTransition(300);
        }
    }

    private void downloadImage(String uri) {
        permissionRequest = new RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        ImageDownloadSubscriber
                                .getImageFromCache(MediaActivity.this, uri)
                                .map(file -> {
                                    FileInputStream in = null;
                                    FileOutputStream out = null;
                                    try {
                                        File newFile = FileHelper.createPublicImageFile(uri);
                                        in = new FileInputStream(file);
                                        out = new FileOutputStream(newFile);
                                        IOUtils.copy(in, out);
                                        return newFile;
                                    } finally {
                                        IOUtils.closeQuietly(in, out);
                                    }
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(file -> {
                                            showMessage(R.string.download_success);
                                            String extension = FilenameUtils.getExtension(file.getName());
                                            MediaScannerConnection.scanFile(
                                                    MediaActivity.this,
                                                    new String[]{file.toString()},
                                                    new String[]{"image/" + extension},
                                                    null);
                                        },
                                        error -> {
                                            showMessage(R.string.download_fail);
                                        });
                    }
                }, error -> showMessage(R.string.download_fail));
    }

    private void shareImage(String uri) {
        ImageDownloadSubscriber
                .getImageFromCache(getApplicationContext(), uri)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(file -> FileHelper.shareImage(this, file),
                        error -> {
                            showMessage(R.string.share_fail);
                        });
    }

    private void showMessage(int resId) {
        Toast.makeText(
                this,
                getString(resId),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        disposeQuietly(permissionRequest);
        super.onDestroy();
    }
}