package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;

import com.blinkseven.jrviewer.util.SpaceItemDecoration;
import com.blinkseven.jrviewer.util.Utils;

public class AutofitRecyclerView extends RecyclerView {
    private StaggeredGridLayoutManager manager;
    private SpaceItemDecoration decoration;

    public AutofitRecyclerView(Context context) {
        super(context);
        init();
    }

    public AutofitRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutofitRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        manager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        decoration = new SpaceItemDecoration(0);
        setLayoutManager(manager);
        addItemDecoration(decoration);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);

        int spanCount = Math.max(1, (int) (Utils.pxToDp(getMeasuredWidth()) / 250));
        decoration.setLeft(spanCount == 1 ? 0 : 10);
        decoration.setRight(spanCount == 1 ? 0 : 10);
        manager.setSpanCount(spanCount);
    }
}