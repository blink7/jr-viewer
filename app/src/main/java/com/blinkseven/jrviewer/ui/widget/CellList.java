package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.adapter.AttachmentAdapter;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.util.SpaceItemDecoration;

import io.realm.RealmList;

public class CellList extends RelativeLayout {
    private Context context;
    private RealmList<ImageModel> imageModels;

    public CellList(Context context) {
        super(context);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.post_multiply_content, this);
    }

    public void setContent(RealmList<ImageModel> imageModels) {
        this.imageModels = imageModels;
        initContent();
    }

    private void initContent() {
        TextView num = findViewById(R.id.num);
        num.setText(context.getString(R.string.images_num, imageModels.size()));

        RecyclerView list = findViewById(R.id.content_list);
        list.setAdapter(new AttachmentAdapter(context, imageModels));
        list.setLayoutManager(new LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false));
        list.addItemDecoration(new SpaceItemDecoration(0, 0, 0, 2));
        list.setHasFixedSize(true);
    }
}