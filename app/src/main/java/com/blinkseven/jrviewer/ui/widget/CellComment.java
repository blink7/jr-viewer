package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.net.CommentSubscriber;
import com.blinkseven.jrviewer.ui.widget.zoomable.TestCellGif;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.ImageDecodeOptionsBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.Collections;

import static com.blinkseven.jrviewer.util.TextUtils.*;

public class CellComment extends LinearLayout {
    private SimpleDraweeView avatar;
    private TextView userName;
    private DateTimeView created;
    private TextView text;
    private FrameLayout attachmentContainer;

    private String parentName;
    private CommentModel commentModel;

    public CellComment(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.comment, this);
        avatar = findViewById(R.id.avatar);
        userName = findViewById(R.id.name);
        created = findViewById(R.id.created);
        text = findViewById(R.id.text);
        attachmentContainer = findViewById(R.id.attachment_container);
    }

    public void setContent(CommentModel commentModel, String parentName) {
        this.commentModel = commentModel;
        this.parentName = parentName;
        initContent();
    }

    private void initContent() {
        ImageDecodeOptions imageDecodeOptions = new ImageDecodeOptionsBuilder()
                .setForceStaticImage(true)
                .build();
        ImageRequest avatarRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(commentModel.getUserAvatar()))
                .setImageDecodeOptions(imageDecodeOptions)
                .build();
        avatar.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(avatar.getController())
                        .setImageRequest(avatarRequest)
                        .build());
        avatar.setOnClickListener(v -> openUserProfile());

        userName.setText(commentModel.getUserName());

        long time = commentModel.getCreated();
        if (time != -1) {
            created.setVisibility(VISIBLE);
            created.setDateTime(time);
        } else {
            created.setVisibility(INVISIBLE);
        }

        text.setMovementMethod(LinkMovementMethod.getInstance());
        String prefix = parentName != null ? "<b>@" + parentName + "</b> " : "";
        text.setText(fromHtml(prefix));

        String hiddenComment = "Нажмите чтобы показать комментарий";
        if (commentModel.getText().contains(hiddenComment)) {
            SpannableStringBuilder ssb = new SpannableStringBuilder(hiddenComment);
            ForegroundColorSpan fcs = new ForegroundColorSpan(Color.parseColor("#BDBDBD"));
            ssb.setSpan(fcs, 0, hiddenComment.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            text.append(ssb);
            text.setOnClickListener(v -> CommentSubscriber.loadCommentById(commentModel.getId()));
        } else {
            text.append(trimTrailingWhitespace(fromHtml(commentModel.getText())));
        }

        ImageModel imageModel = commentModel.getImage();
        if (imageModel != null) {
            attachmentContainer.setVisibility(VISIBLE);

            if(attachmentContainer.getTag() != null
                    && attachmentContainer.getTag().equals(imageModel.getUri())) {
                return;
            }
            attachmentContainer.setTag(imageModel.getUri());
            attachmentContainer.removeAllViews();

            switch (imageModel.getType()) {
                case IMG:
                    CellImage cellImage = new CellImage(getContext());
                    cellImage.setContent(
                            Collections.singletonList(imageModel),
                            0,
                            CellImage.VERTICAL);
                    attachmentContainer.addView(cellImage);
                    break;
                case GIF:
                    TestCellGif cellGif = new TestCellGif(getContext());
                    cellGif.setContent(
                            Collections.singletonList(imageModel),
                            0,
                            CellImage.VERTICAL);
                    attachmentContainer.addView(cellGif);
                    break;
                default:
                    CellVideo cellVideo = new CellVideo(getContext());
                    cellVideo.setContent(imageModel, CellImage.VERTICAL);
                    attachmentContainer.addView(cellVideo);
            }
        } else {
            attachmentContainer.setVisibility(GONE);
        }
    }

    public void openUserProfile() {
        UserModel user = new UserModel();
        user.setUserName(commentModel.getUserName());
        user.setUserAvatar(commentModel.getUserAvatar());
        EventBus.getDefault().post(user);
    }
}