package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.ui.MediaActivity_;
import com.blinkseven.jrviewer.util.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.fresco.animation.drawable.AnimatedDrawable2;
import com.facebook.fresco.animation.drawable.AnimationListener;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;

import java.util.ArrayList;
import java.util.List;

public class CellGif extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = CellGif.class.getSimpleName();

    private DynamicImageView image;
    private ImageView playback;
    private TextView type;

    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    int orientation;

    private List<ImageModel> imageModels;
    private int position;

    private ProgressBarDrawable progressBarDrawable;

    public CellGif(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.post_content, this);
        image = findViewById(R.id.image);
        playback = findViewById(R.id.playback);
        type = findViewById(R.id.type);

        playback.setOnClickListener(v -> onPlayGif());
        setOnClickListener(this);

        progressBarDrawable = new ProgressBarDrawable();
        progressBarDrawable.setBarWidth(Utils.dpToPx(4));
        //TODO: Set color.
        progressBarDrawable.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        progressBarDrawable.setBackgroundColor(Color.parseColor("#4D000000"));
        progressBarDrawable.setPadding(0);
    }

    public void setContent(List<ImageModel> imageModels, int position, int orientation) {
        this.imageModels = imageModels;
        this.position = position;
        this.orientation = orientation;
        initContent();
    }

    private void initContent() {
        playback.setVisibility(GONE);
        type.setText("GIF");

        ImageModel imageModel = imageModels.get(position);
        if (orientation == VERTICAL) {
            image.setHeightRatio(imageModel.getHeight() / (float) imageModel.getWidth());
        } else {
            image.setWidthRatio(imageModel.getWidth() / (float) imageModel.getHeight());
        }

        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                playback.setVisibility(VISIBLE);
                type.setVisibility(VISIBLE);
            }
        };
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setLowResImageRequest(ImageRequest.fromUri(imageModel.getUri()))
                .setUri(imageModel.getFullUri())
                .setControllerListener(controllerListener)
                .setTapToRetryEnabled(true)
                .setOldController(image.getController())
                .build();
        image.setController(controller);
        image.getHierarchy().setProgressBarImage(progressBarDrawable);
    }

    public void onPlayGif() {
        AnimatedDrawable2 animatable = (AnimatedDrawable2) image.getController().getAnimatable();
        if (animatable != null) {
            animatable.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(AnimatedDrawable2 drawable) {
                    playback.setVisibility(GONE);
                    type.setVisibility(GONE);
                }

                @Override
                public void onAnimationStop(AnimatedDrawable2 drawable) {
                    playback.setVisibility(VISIBLE);
                    type.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationReset(AnimatedDrawable2 drawable) {
                }

                @Override
                public void onAnimationRepeat(AnimatedDrawable2 drawable) {
                }

                @Override
                public void onAnimationFrame(AnimatedDrawable2 drawable, int frameNumber) {
                    if (frameNumber == drawable.getFrameCount() - 1) {
                        drawable.stop();
                    }
                }
            });
            animatable.start();
        }
    }

    @Override
    public void onClick(View view) {
        int startPosition = 0;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < imageModels.size(); i++) {
            ImageModel imageModel = imageModels.get(i);
            if (imageModel.getType() == ImageModel.ImageType.IMG) {
                list.add(imageModel.getUri());
            } else if (imageModel.getType() == ImageModel.ImageType.GIF) {
                list.add(imageModel.getFullUri());
            }

            if (i == position) {
                startPosition = list.size() - 1;
            }
        }

        MediaActivity_
                .intent(getContext())
                .extra("uris", list.toArray(new String[list.size()]))
                .extra("position", startPosition)
                .start();
    }
}