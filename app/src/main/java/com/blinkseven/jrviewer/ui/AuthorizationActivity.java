package com.blinkseven.jrviewer.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.events.OnErrorLoader;
import com.blinkseven.jrviewer.events.OnSuccessEvent;
import com.blinkseven.jrviewer.net.UserSubscriber;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.blinkseven.jrviewer.util.Utils.*;

@EActivity(R.layout.activity_authorization)
public class AuthorizationActivity extends AppCompatActivity {

    public static final int CODE = 7;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.login)
    AutoCompleteTextView loginView;
    @ViewById(R.id.password)
    EditText passwordView;
    @ViewById(R.id.progress_authorization_box)
    View progressBox;
    @ViewById(R.id.login_form)
    View loginFormView;

    @Extra
    Boolean fromDrawer;

    private final EventBus bus = EventBus.getDefault();
    private Disposable subscription;
    private Realm realmInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
        realmInstance = Realm.getDefaultInstance();
    }

    @AfterViews
    public void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            // Add back arrow to toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setTitle(R.string.authorization_activity_title);
        }

        RealmResults<UserModel> currentUsers = realmInstance
                .where(UserModel.class)
                .equalTo("currentUser", true)
                .findAll();
        if (!currentUsers.isEmpty()) {
            List<String> names = new ArrayList<>();
            for (UserModel userModel : currentUsers) {
                names.add(userModel.getUserName());
            }
            addNamesToAutoComplete(names);
        }
        passwordView.setOnEditorActionListener((v, id, event) -> {
            if (id == R.id.login || id == EditorInfo.IME_ACTION_UNSPECIFIED) {
                attemptLogin();
                return true;
            }
            return false;
        });
    }

    private void addNamesToAutoComplete(List<String> namesCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this,
                        android.R.layout.simple_dropdown_item_1line, namesCollection);

        loginView.setAdapter(adapter);
    }

    @Click(R.id.sign_in_button)
    public void attemptLogin() {
        // Reset errors
        loginView.setError(null);
        passwordView.setError(null);

        String login = loginView.getText().toString();
        String password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid login
        if (login.trim().isEmpty()) {
            loginView.setError(getString(R.string.error_field_required));
            focusView = loginView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one
        if (password.trim().isEmpty()) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt
            showProgress(loginFormView, progressBox, true);
            subscription = UserSubscriber.authorization(login.trim(), password.trim());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessEvent(OnSuccessEvent event) {
        showProgress(loginFormView, progressBox, false);
        if (fromDrawer != null && fromDrawer) {
            setResult(RESULT_OK);
            finish();
        } else {
            MainActivity_.intent(this).start();
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(OnErrorLoader event) {
        showProgress(loginFormView, progressBox, false);
        Toast.makeText(this, event.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
    }

    @OptionsItem(android.R.id.home)
    boolean menuHome() {
        finish();
        return true;
    }

    @Override
    protected void onDestroy() {
        disposeQuietly(subscription);
        bus.unregister(this);
        realmInstance.close();
        super.onDestroy();
    }
}