package com.blinkseven.jrviewer.ui;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.widget.Toast;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.adapter.PostAdapter;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.events.OnErrorPosts;
import com.blinkseven.jrviewer.events.OnUpBtnEvent;
import com.blinkseven.jrviewer.events.PostsEvent;
import com.blinkseven.jrviewer.ui.widget.AutofitRecyclerView;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.disposables.Disposable;

import static com.blinkseven.jrviewer.util.Utils.disposeQuietly;
import static com.blinkseven.jrviewer.net.PostsSubscriber.*;

@EFragment(R.layout.fragment_posts)
public class PostsFragment
        extends Fragment
        implements PostAdapter.OnItemClickListener {

    public static final String TAG = PostsFragment.class.getSimpleName();

    @FragmentArg
    PostType type;
    @FragmentArg
    String value;

    @ViewById(R.id.list)
    AutofitRecyclerView list;
    @ViewById(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @InstanceState
    boolean isNewInstance = true;
    @InstanceState
    int page = 0;
    @InstanceState
    int pageNext = 0;
    @InstanceState
    String redirectionUrl;

    private final EventBus bus = EventBus.getDefault();
    private Disposable subscription;

    private Handler handler = new Handler();

    public static PostsFragment getInstance(@NonNull PostType type) {
        return PostsFragment_.builder().type(type).build();
    }

    public static PostsFragment getInstance(@NonNull PostType type, @NonNull String value) {
        return PostsFragment_.builder().type(type).value(value).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
        if (isNewInstance) {
            updateData();
            isNewInstance = false;
        }
    }

    @AfterViews
    public void init() {
        setupSwipeRefresh();
        setupList();
        setupInfiniteScroll();
    }

    private void setupList() {
        list.setAdapter(new PostAdapter(type));
        ((PostAdapter) list.getAdapter()).setOnItemClickListener(this);
        list.setHasFixedSize(true);
        RecyclerView.ItemAnimator animator = list.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
//        list.setItemViewCacheSize(20);
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, ">> dy " + dy);
                if (dy > 0) {
                    EventBus.getDefault().post(new OnUpBtnEvent(false));
                } else {
                    EventBus.getDefault().post(new OnUpBtnEvent(true));
                }
            }
        });
    }

    private void setupInfiniteScroll() {
        BaseAttacher attacher = Mugen.with(list, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                if (pageNext != page) {
                    handler.post(PostsFragment.this::initLoader);
                }
            }

            @Override
            public boolean isLoading() {
                if (subscription != null) {
                    return !subscription.isDisposed();
                }
                return false;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return page == 1;
            }
        });
        attacher.setLoadMoreEnabled(true);
        attacher.setLoadMoreOffset(6);
        attacher.start();
    }

    private void setupSwipeRefresh() {
        swipeRefresh.setDistanceToTriggerSync(550);
        swipeRefresh.setOnRefreshListener(() -> refreshData(0));
        //TODO: Set color.
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
    }

    private void refreshData(int page) {
        switch (type) {
            case GOOD:
                subscription = loadGoodPosts(page);
                break;
            case BEST:
                subscription = loadBestPosts(page);
                break;
            case ALL:
                subscription = loadAllPosts(page);
                break;
            case ABYSS:
                subscription = loadAbyssPosts(page);
                break;
            case TAG:
                if (redirectionUrl == null) {
                    subscription = loadTagPosts(page, value);
                } else {
                    subscription = loadTagPostsWithUrl(page, redirectionUrl);
                }
                break;
            case USER:
                subscription = loadUserPosts(page, value);
                break;
            case FAVORITE:
                subscription = loadFavoritePosts(page, value);
                break;
            case FRIENDLINE:
                subscription = loadFriendlinePosts(page, value);
                break;
            default:
                subscription = null;
        }
    }

    private void initLoader() {
        if (pageNext >= 0) {
            refreshData(pageNext);
        } else {
            pageNext = 0;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPostsEvent(final PostsEvent event) {
        swipeRefresh.setRefreshing(false);
        page = event.getPage();
        pageNext = page - 1;

        if (type == PostType.TAG && redirectionUrl == null) {
            redirectionUrl = event.getRedirectionUrl();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(final OnErrorPosts event) {
        Toast.makeText(getContext(), event.getMessage(), Toast.LENGTH_SHORT).show();
        swipeRefresh.setRefreshing(false);
    }

    public void updateData() {
        if (page < 0) {
            page = 0;
        }
        pageNext = page;
        swipeRefresh.post(() -> {
            swipeRefresh.setRefreshing(true);
            initLoader();
        });
    }

    @Override
    public void onItemClick(String id) {
        PostDetailActivity_.intent(getContext()).postId(id).postType(type).start();
    }

    public void upList() {
        list.scrollToPosition(0);
        EventBus.getDefault().post(new OnUpBtnEvent(false));
    }

    @Override
    public void onPause() {
        if (swipeRefresh != null) {
            swipeRefresh.setRefreshing(false);
            swipeRefresh.destroyDrawingCache();
            swipeRefresh.clearAnimation();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        disposeQuietly(subscription);
        bus.unregister(this);
        super.onStop();
    }
}