package com.blinkseven.jrviewer.ui;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.events.OnUpBtnEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseActivity extends AppCompatActivity {

    private final EventBus bus = EventBus.getDefault();

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        bus.unregister(this);
        super.onPause();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserProfileEvent(final UserModel user) {
        showUserSheet(getSupportFragmentManager(), user);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpButtonEvent(final OnUpBtnEvent event) {
        showUpButton(event.isShow());
    }

    public void showUserSheet(FragmentManager fm, UserModel user) {
        UserBottomSheet userSheet = new UserBottomSheet();
        userSheet.setUser(user);
        userSheet.show(fm, UserBottomSheet.class.getSimpleName());
    }

    public void showUpButton(boolean show) {}
}