package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.db.model.PollModel;
import com.blinkseven.jrviewer.db.model.PollOptionModel;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.db.model.TagModel;
import com.blinkseven.jrviewer.db.model.UserModel;
import com.blinkseven.jrviewer.ui.widget.zoomable.TestCellGif;
import com.blinkseven.jrviewer.util.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.ImageDecodeOptionsBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.flexbox.FlexboxLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.realm.RealmList;

import static com.blinkseven.jrviewer.util.TextUtils.*;

public class CellPost extends RelativeLayout {

    private static final String TAG = CellPost.class.getSimpleName();

    private CardView card;
    private SimpleDraweeView avatar;
    private TextView userName;
    private DateTimeView created;
    private TextView text;
    private FlexboxLayout tagContainer;
    private LinearLayout poll;
    private TextView pollTitle;
    private FrameLayout contentContainer;
    private TextView rating;
    private TextView commentNum;

    private PostDataModel postData;
    private boolean fullMode;

    public CellPost(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.post, this);
        card = findViewById(R.id.card);
        avatar = findViewById(R.id.avatar);
        userName = findViewById(R.id.name);
        created = findViewById(R.id.created);
        text = findViewById(R.id.text);
        tagContainer = findViewById(R.id.tag_container);
        poll = findViewById(R.id.poll_container);
        pollTitle = findViewById(R.id.poll_title);
        contentContainer = findViewById(R.id.post_container);
        rating = findViewById(R.id.like_num);
        commentNum = findViewById(R.id.comments_num);
    }

    public void setData(PostDataModel postData, boolean fullMode) {
        this.postData = postData;
        this.fullMode = fullMode;
        initData();
    }

    @SuppressWarnings("deprecation")
    void initData() {
        if (fullMode) {
            ((LayoutParams) card.getLayoutParams()).setMargins(0, 0, 0 , Utils.dpToPx(8));
            card.setCardElevation(0);

            text.setMovementMethod(LinkMovementMethod.getInstance());
            text.setMaxLines(Integer.MAX_VALUE);
            text.setEllipsize(null);
        }

        ImageDecodeOptions imageDecodeOptions = new ImageDecodeOptionsBuilder()
                .setForceStaticImage(true)
                .build();
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(postData.getUserAvatar()))
                .setImageDecodeOptions(imageDecodeOptions)
                .build();
        avatar.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(avatar.getController())
                        .setImageRequest(request)
                        .build());
        avatar.setOnClickListener(v -> openUserProfile());

        userName.setText(postData.getUserName());

        created.setFullMode(fullMode);
        created.setDateTime(postData.getCreated());

        if (postData.getText().isEmpty()) {
            text.setVisibility(View.GONE);
        } else {
            text.setVisibility(View.VISIBLE);
            text.setText(trimTrailingWhitespace(fromHtml(postData.getText())));
        }

        // Configure tags cell
        fillTags(postData.getTags());

        // Configure poll cell
        PollModel pollModel = postData.getPoll();
        if (pollModel != null) {
            poll.setVisibility(VISIBLE);
            if (poll.getChildCount() > 1) {
                poll.removeViews(1, poll.getChildCount() - 1);
            }

            pollTitle.setText(pollModel.getTitle());
            for (PollOptionModel option : pollModel.getOptions()) {
                CellPollOption cellPollOption = new CellPollOption(getContext());
                cellPollOption.setContent(option);
                LayoutParams layoutParams = new LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, Utils.dpToPx(6));
                cellPollOption.setLayoutParams(layoutParams);
                poll.addView(cellPollOption);
            }
        } else {
            poll.setVisibility(GONE);
        }

        // Configure content cell
        RealmList<ImageModel> content = postData.getContent();
        if (content.isEmpty()) {
            contentContainer.setVisibility(View.GONE);
        } else if (content.size() == 1){
            contentContainer.setVisibility(View.VISIBLE);

            ImageModel imageModel = content.get(0);

            if(contentContainer.getTag() != null
                    && contentContainer.getTag().equals(imageModel.getUri())) {
                return;
            }
            contentContainer.setTag(imageModel.getUri());
            contentContainer.removeAllViews();

            switch (imageModel.getType()) {
                case IMG:
                    CellImage cellImage = new CellImage(getContext());
                    cellImage.setContent(content, 0, CellImage.VERTICAL);
                    contentContainer.addView(cellImage);
                    break;
                case GIF:
                    TestCellGif cellGif = new TestCellGif(getContext());
                    cellGif.setContent(content, 0, CellImage.VERTICAL);
                    contentContainer.addView(cellGif);
                    break;
                default:
                    CellVideo cellVideo = new CellVideo(getContext());
                    cellVideo.setContent(imageModel, CellImage.VERTICAL);
                    contentContainer.addView(cellVideo);
            }
        } else {
            contentContainer.setVisibility(View.VISIBLE);

            if(contentContainer.getTag() != null
                    && contentContainer.getTag().equals(content.get(0).getUri())) {
                return;
            }
            contentContainer.setTag(content.get(0).getUri());
            contentContainer.removeAllViews();

            CellList cellList = new CellList(getContext());
            cellList.setContent(content);
            contentContainer.addView(cellList);
        }

        if (pollModel == null && content.isEmpty()) {
            text.setMaxLines(20);
        }

        setRating(postData.getRating());
        setCommentsNum(postData.getCommentNum());
    }

    private void fillTags(List<TagModel> tagModels) {
        tagContainer.removeAllViews();
        if (!tagModels.isEmpty()) {
            tagContainer.setVisibility(VISIBLE);
            for (TagModel tagModel : tagModels) {
                CellTag cellTag = new CellTag(getContext());
                cellTag.setTagName(tagModel.getName(), fullMode);
                tagContainer.addView(cellTag);
            }
        } else {
            tagContainer.setVisibility(GONE);
        }
    }

    private void setRating(float value) {
        if (value != 0.0f) {
            rating.setVisibility(VISIBLE);
            rating.setText(String.valueOf(value));
        } else {
            rating.setVisibility(GONE);
        }
    }

    private void setCommentsNum(int value) {
        if (value != 0) {
            commentNum.setVisibility(VISIBLE);
            commentNum.setText(String.valueOf(value));
        } else {
            commentNum.setVisibility(GONE);
        }
    }

    public void updateStats(PostDataModel postData) {
        fillTags(postData.getTags());
        setRating(postData.getRating());
        setCommentsNum(postData.getCommentNum());
    }

    public void openUserProfile() {
        UserModel user = new UserModel();
        user.setUserName(postData.getUserName());
        user.setUserAvatar(postData.getUserAvatar());
        EventBus.getDefault().post(user);
    }
}