package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.ui.MediaActivity_;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.blinkseven.jrviewer.util.Utils.*;

public class CellImage extends RelativeLayout implements View.OnClickListener {
    private static final String TAG = CellImage.class.getSimpleName();

    private DynamicImageView imageView;

    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    int orientation;

    private List<ImageModel> images;
    private int position;

    public CellImage(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.post_content, this);
        imageView = findViewById(R.id.image);
        findViewById(R.id.playback).setVisibility(GONE);
        findViewById(R.id.type).setVisibility(GONE);

        setOnClickListener(this);
    }

    public void setContent(List<ImageModel> imageModels, int position, int orientation) {
        this.images = imageModels;
        this.position = position;
        this.orientation = orientation;
        initContent();
    }

    private void initContent() {
        ImageModel image = images.get(position);

        int height = image.getHeight() > getDisplayHeight()
                ? getDisplayHeight() : image.getHeight();
        int width = image.getWidth();
        if (orientation == VERTICAL) {
//            imageView.setHeightRatio(height / (float) width);
            float screenRatio = getDisplayHeight() / (float) getDisplayWidth();
            float imageRation = image.getHeight() / (float) image.getWidth();
            if (imageRation <= screenRatio) {
                imageView.setHeightRatio(imageRation);
            } else {
                imageView.setHeightRatio(screenRatio);
            }
        } else {
            imageView.setWidthRatio(width / (float) height);
        }

        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(image.getUri()))
                .setResizeOptions(new ResizeOptions(width, height))
                .build();
        imageView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(imageView.getController())
                        .setImageRequest(request)
                        .build());
    }

    @Override
    public void onClick(View view) {
        int startPosition = 0;
        List<String> images = new ArrayList<>();
        for (int i = 0; i < this.images.size(); i++) {
            ImageModel imageModel = this.images.get(i);
            if (imageModel.getType() == ImageModel.ImageType.IMG) {
                images.add(imageModel.getUri());
            } else if (imageModel.getType() == ImageModel.ImageType.GIF) {
                images.add(imageModel.getFullUri());
            }

            if (i == position) {
               startPosition = images.size() - 1;
            }
        }

        MediaActivity_
                .intent(getContext())
                .uris(images.toArray(new String[images.size()]))
                .position(startPosition)
                .start();
    }
}