package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.PollOptionModel;

public class CellPollOption extends RelativeLayout {
    private ProgressBar progress;
    private TextView title;
    private TextView percent;

    private PollOptionModel option;

    public CellPollOption(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.poll_option, this);
        progress = findViewById(R.id.progress_votes);
        title = findViewById(R.id.option_title);
        percent = findViewById(R.id.option_percent);
    }

    public void setContent(PollOptionModel option) {
        this.option = option;
        initContent();
    }

    private void initContent() {
        progress.setProgress((int) option.getPercent());
        title.setText(option.getName());
        String percentValue = (int) option.getPercent() + "%";
        percent.setText(percentValue);
    }
}