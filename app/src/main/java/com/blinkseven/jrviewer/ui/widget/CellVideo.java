package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.facebook.drawee.drawable.ScalingUtils;

import java.util.List;

public class CellVideo extends RelativeLayout implements View.OnClickListener {
    private DynamicImageView image;
    private TextView type;

    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    int orientation;

    private ImageModel imageModel;

    public CellVideo(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.post_content, this);
        image = findViewById(R.id.image);
        type = findViewById(R.id.type);

        setOnClickListener(this);
    }

    public void setContent(ImageModel imageModel, int orientation) {
        this.imageModel = imageModel;
        this.orientation = orientation;
        initContent();
    }

    private void initContent() {
        type.setText(imageModel.getType().toString());

        if (orientation == VERTICAL) {
            image.setHeightRatio(imageModel.getHeight() / (float) imageModel.getWidth());
        } else {
            image.setWidthRatio(imageModel.getWidth() / (float) imageModel.getHeight());
        }
        image.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
        image.setImageURI(imageModel.getUri());
    }

    @Override
    public void onClick(View view) {
        String fullUri = imageModel.getFullUri();
        if (fullUri != null && !fullUri.isEmpty()) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(fullUri));
            getContext().startActivity(intent);
        }
    }
}