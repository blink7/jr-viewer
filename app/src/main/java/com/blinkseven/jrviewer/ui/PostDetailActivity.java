package com.blinkseven.jrviewer.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.adapter.CommentAdapter;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;
import com.blinkseven.jrviewer.events.OnErrorPostDetail;
import com.blinkseven.jrviewer.events.PostDetailEvent;
import com.blinkseven.jrviewer.net.PostsSubscriber;
import com.blinkseven.jrviewer.util.DividerItemDecoration;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.disposables.Disposable;
import io.realm.Realm;

import static com.blinkseven.jrviewer.util.Utils.disposeQuietly;

@EActivity(R.layout.activity_post_detail)
public class PostDetailActivity extends BaseActivity {

    private static final String TAG = PostDetailActivity.class.getSimpleName();

    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @ViewById(R.id.list)
    RecyclerView list;

    @InstanceState
    boolean isNewInstance = true;

    @Extra
    String postId;
    @Extra
    PostType postType;

    private PostDataModel data;

    private Disposable subscription;
    private Realm realmInstance;

    private CommentAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realmInstance = Realm.getDefaultInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isNewInstance) {
            isNewInstance = false;
            realmInstance.executeTransaction(realm -> realm.delete(CommentModel.class));
            update();
        }
    }

    @AfterViews
    public void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            // Add back arrow to toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        setupSwipeRefresh();

        if (data == null) {
            switch (postType) {
                case GOOD:
                    data = realmInstance.where(PostGoodModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
                case BEST:
                    data = realmInstance.where(PostBestModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
                case ALL:
                    data = realmInstance.where(PostAllModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
                case ABYSS:
                    data = realmInstance.where(PostAbyssModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
                case TAG:
                    data = realmInstance.where(PostTagModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
                default:
                    data = realmInstance.where(PostUserModel.class)
                            .equalTo("id", postId).findFirst().getData();
                    break;
            }
        }

        setupCommentsList();
    }

    private void setupSwipeRefresh() {
        swipeRefresh.setDistanceToTriggerSync(550);
        swipeRefresh.setOnRefreshListener(this::refreshData);
        //TODO: Set color.
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
    }

    private void setupCommentsList() {
        adapter = new CommentAdapter(data);
        list.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setAutoMeasureEnabled(true);
        list.setHasFixedSize(true);
        list.setLayoutManager(layoutManager);
        list.addItemDecoration(new DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL,
                1));
        RecyclerView.ItemAnimator animator = list.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
    }

    private void update() {
        swipeRefresh.post(() -> {
            swipeRefresh.setRefreshing(true);
            refreshData();
        });
    }

    private void refreshData() {
        subscription = PostsSubscriber.loadPostById(postId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPostEvent(final PostDetailEvent event) {
        swipeRefresh.postDelayed(() -> {
            realmInstance.refresh();
            adapter.notifyItemChanged(0);
            swipeRefresh.setRefreshing(false);
        }, 1000);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(final OnErrorPostDetail event) {
        event.getError().printStackTrace();
        Toast.makeText(this, event.getError().getMessage(), Toast.LENGTH_SHORT).show();
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_detail_activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                update();
                return true;
            case R.id.share_link:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "http://joyreactor.cc/post/" + postId);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
            case R.id.open_in_browser:
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://joyreactor.cc/post/" + postId)));
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        if (swipeRefresh != null) {
            swipeRefresh.setRefreshing(false);
            swipeRefresh.destroyDrawingCache();
            swipeRefresh.clearAnimation();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        disposeQuietly(subscription);
        realmInstance.close();
        super.onDestroy();
    }
}