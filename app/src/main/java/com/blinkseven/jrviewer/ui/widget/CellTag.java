package com.blinkseven.jrviewer.ui.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.ui.DataActivity_;
import com.blinkseven.jrviewer.util.TextUtils;

public class CellTag extends LinearLayout implements View.OnClickListener {

    TextView tag;

    String tagName;

    public CellTag(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.tag, this);
        tag = findViewById(R.id.tag);
    }

    public void setTagName(String tagName, boolean fullMode) {
        if (fullMode) {
            tag.setOnClickListener(this);
        }

        this.tagName = tagName;
        String prefix = tagName.charAt(0) == '#' ? "" : "#";
        tag.setText(TextUtils.fromHtml(String.format("<u>%s</u> ", prefix + tagName)));
    }

    @Override
    public void onClick(View view) {
        DataActivity_
                .intent(getContext())
                .type(PostType.TAG)
                .value(tagName)
                .start();
    }
}