package com.blinkseven.jrviewer.ui;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.blinkseven.jrviewer.R;
import com.blinkseven.jrviewer.db.PostType;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_tag)
public class DataActivity extends BaseActivity {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @Extra
    PostType type;
    @Extra
    String value;

    @InstanceState
    boolean isNewInstance = true;

    private FragmentManager fm = getSupportFragmentManager();
    private PostsFragment fragment;

    @AfterViews
    public void init() {
        if (isNewInstance) {
            isNewInstance = false;
            fragment = PostsFragment.getInstance(type, value);
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        switch (type) {
            case TAG:
                setTitle(value);
                break;
            case USER:
                setTitle(value);
                break;
            case FAVORITE:
                setTitle(getString(R.string.favorite) + " " + value);
                break;
            case FRIENDLINE:
                setTitle(getString(R.string.friendline) + " " + value);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}