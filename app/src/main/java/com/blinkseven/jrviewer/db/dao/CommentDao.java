package com.blinkseven.jrviewer.db.dao;

import com.blinkseven.jrviewer.db.model.CommentModel;

import io.realm.Realm;
import io.realm.RealmResults;

public class CommentDao {

    public static RealmResults getComments() {
        return Realm.getDefaultInstance()
                .where(CommentModel.class)
                .findAll();
    }
}