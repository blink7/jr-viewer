package com.blinkseven.jrviewer.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ImageModel extends RealmObject {
    private String type;
    private String fullUri;
    @PrimaryKey
    private String uri;
    private int width;
    private int height;

    public ImageType getType() {
        return type != null ? ImageType.valueOf(type) : null;
    }

    public void setType(ImageType type) {
        this.type = type.toString();
    }

    public String getFullUri() {
        return fullUri;
    }

    public void setFullUri(String fullUri) {
        this.fullUri = fullUri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public enum ImageType {
        IMG,
        GIF,
        YouTube,
        Coub,
        vimeo
    }
}