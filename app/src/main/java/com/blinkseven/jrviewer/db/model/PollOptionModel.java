package com.blinkseven.jrviewer.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class PollOptionModel extends RealmObject {
    @PrimaryKey
    private String name;
    private int count;
    private float percent;

    public PollOptionModel() {
    }

    public PollOptionModel(String name, int count, float percent) {
        this.name = name;
        this.count = count;
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }
}