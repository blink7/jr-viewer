package com.blinkseven.jrviewer.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class PostAbyssModel extends RealmObject {
    @PrimaryKey
    private String id;
    private PostDataModel data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PostDataModel getData() {
        return data;
    }

    public void setData(PostDataModel data) {
        this.data = data;
    }
}