package com.blinkseven.jrviewer.db.dao;

import com.blinkseven.jrviewer.db.PostType;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostFavoriteModel;
import com.blinkseven.jrviewer.db.model.PostFriendlineModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;

import io.realm.Realm;
import io.realm.RealmResults;

public class PostDao {

    private static RealmResults<PostGoodModel> getGood() {
        return Realm.getDefaultInstance()
                .where(PostGoodModel.class)
                .findAll();
    }

    private static RealmResults<PostAllModel> getAll() {
        return Realm.getDefaultInstance()
                .where(PostAllModel.class)
                .findAll();
    }

    private static RealmResults<PostBestModel> getBest() {
        return Realm.getDefaultInstance()
                .where(PostBestModel.class)
                .findAll();
    }

    private static RealmResults<PostAbyssModel> getAbyss() {
        return Realm.getDefaultInstance()
                .where(PostAbyssModel.class)
                .findAll();
    }

    private static RealmResults<PostTagModel> getTag() {
        return Realm.getDefaultInstance()
                .where(PostTagModel.class)
                .findAll();
    }

    private static RealmResults<PostUserModel> getByUser() {
        return Realm.getDefaultInstance()
                .where(PostUserModel.class)
                .findAll();
    }

    private static RealmResults<PostFavoriteModel> getFavorite() {
        return Realm.getDefaultInstance()
                .where(PostFavoriteModel.class)
                .findAll();
    }

    private static RealmResults<PostFriendlineModel> getFriendline() {
        return Realm.getDefaultInstance()
                .where(PostFriendlineModel.class)
                .findAll();
    }

    public static RealmResults getByType(PostType type) {
        switch (type) {
            case GOOD:
                return getGood();
            case ALL:
                return getAll();
            case BEST:
                return getBest();
            case ABYSS:
                return getAbyss();
            case TAG:
                return getTag();
            case USER:
                return getByUser();
            case FAVORITE:
                return getFavorite();
            case FRIENDLINE:
                return getFriendline();
            default:
                return null;
        }
    }
}