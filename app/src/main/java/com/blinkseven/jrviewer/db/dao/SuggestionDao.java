package com.blinkseven.jrviewer.db.dao;

import com.blinkseven.jrviewer.db.model.SearchSuggestionModel;

import io.realm.Realm;

public class SuggestionDao {

    public static void addSuggestion(String query) {
        Realm realmInst = Realm.getDefaultInstance();
        realmInst.executeTransactionAsync(
                realm -> realm.copyToRealmOrUpdate(new SearchSuggestionModel(query)));
        realmInst.close();
    }
}