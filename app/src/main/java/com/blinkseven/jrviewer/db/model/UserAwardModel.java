package com.blinkseven.jrviewer.db.model;


import org.parceler.Parcel;

import org.parceler.Parcels;

import io.realm.RealmObject;
import io.realm.UserAwardModelRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@Parcel(implementations = {UserAwardModelRealmProxy.class},
        value = org.parceler.Parcel.Serialization.BEAN,
        analyze = {UserModel.class})
@RealmClass
public class UserAwardModel extends RealmObject {
    @PrimaryKey
    private String awardUrn;
    private String imageUri;
    private String description;

    public String getAwardUrn() {
        return awardUrn;
    }

    public void setAwardUrn(String awardUrn) {
        this.awardUrn = awardUrn;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class RealmListParcelConverter
            extends com.blinkseven.jrviewer.db.model.RealmListParcelConverter<UserAwardModel> {

        @Override
        public void itemToParcel(UserAwardModel input, android.os.Parcel parcel) {
            parcel.writeParcelable(Parcels.wrap(UserAwardModel.class, input), 0);
        }

        @Override
        public UserAwardModel itemFromParcel(android.os.Parcel parcel) {
            return Parcels.unwrap(parcel.readParcelable(UserAwardModel.class.getClassLoader()));
        }
    }
}