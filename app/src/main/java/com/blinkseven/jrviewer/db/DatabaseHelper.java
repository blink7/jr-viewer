package com.blinkseven.jrviewer.db;

import com.blinkseven.jrviewer.db.model.AuthorizationModel;
import com.blinkseven.jrviewer.db.model.CommentModel;
import com.blinkseven.jrviewer.db.model.ImageModel;
import com.blinkseven.jrviewer.db.model.PollModel;
import com.blinkseven.jrviewer.db.model.PollOptionModel;
import com.blinkseven.jrviewer.db.model.PostAbyssModel;
import com.blinkseven.jrviewer.db.model.PostAllModel;
import com.blinkseven.jrviewer.db.model.PostBestModel;
import com.blinkseven.jrviewer.db.model.PostDataModel;
import com.blinkseven.jrviewer.db.model.PostDetailModel;
import com.blinkseven.jrviewer.db.model.PostFavoriteModel;
import com.blinkseven.jrviewer.db.model.PostFriendlineModel;
import com.blinkseven.jrviewer.db.model.PostGoodModel;
import com.blinkseven.jrviewer.db.model.PostUserModel;
import com.blinkseven.jrviewer.db.model.PostTagModel;
import com.blinkseven.jrviewer.db.model.SearchSuggestionModel;
import com.blinkseven.jrviewer.db.model.TagModel;
import com.blinkseven.jrviewer.db.model.UserAwardModel;
import com.blinkseven.jrviewer.db.model.UserModel;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

public class DatabaseHelper {
    @RealmModule(classes = {PostDetailModel.class, PostBestModel.class, PostGoodModel.class,
            PostAllModel.class, PostAbyssModel.class, TagModel.class, ImageModel.class,
            CommentModel.class, PollModel.class, PollOptionModel.class, PostDataModel.class,
            PostUserModel.class, PostTagModel.class, SearchSuggestionModel.class,
            UserModel.class, AuthorizationModel.class, UserAwardModel.class,
            PostFavoriteModel.class, PostFriendlineModel.class})

    private static class BaseModule {
    }

    private static final String ENCRYPTION_KEY = "F>a;RC_;O`+:]v?v_}r@6gReaK9h7zX3Jv7H{#Rj}!CtmFJHL@O:CZ}<8PPwhX72";

    public static void configure(){
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jrviewer.realm")
                .encryptionKey(ENCRYPTION_KEY.getBytes())
                .modules(new BaseModule())
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);
    }
}