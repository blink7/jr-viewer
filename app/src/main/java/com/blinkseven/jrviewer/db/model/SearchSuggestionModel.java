package com.blinkseven.jrviewer.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class SearchSuggestionModel extends RealmObject {
    @PrimaryKey
    private String name;

    public SearchSuggestionModel() {
    }

    public SearchSuggestionModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchSuggestionModel suggestion = (SearchSuggestionModel) o;

        return name.equals(suggestion.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}