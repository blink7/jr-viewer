package com.blinkseven.jrviewer.db;

public enum PostType {
    GOOD,
    ALL,
    BEST,
    ABYSS,
    DETAIL,
    TAG,
    USER,
    FAVORITE,
    FRIENDLINE
}