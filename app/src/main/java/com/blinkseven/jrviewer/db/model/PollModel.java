package com.blinkseven.jrviewer.db.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class PollModel extends RealmObject {
    @PrimaryKey
    private String title;
    private RealmList<PollOptionModel> options;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RealmList<PollOptionModel> getOptions() {
        return options;
    }

    public void setOptions(RealmList<PollOptionModel> options) {
        this.options = options;
    }
}