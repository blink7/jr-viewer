package com.blinkseven.jrviewer.db.model;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.UserModelRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@Parcel(implementations = {UserModelRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {UserModel.class})
@RealmClass
public class UserModel extends RealmObject {
    @PrimaryKey
    private String userName;
    private String userAvatar;

    private RealmList<UserAwardModel> awards;

    private int stars;
    private int progress;
    private String ratingText;
    private String ratingTotal;
    private String ratingWeek;

    private String totalPostsNumber;
    private String goodPostsNumber;
    private String bestPostsNumber;
    private String commentsNumber;
    private String since;
    private String lastVisit;
    private String consecutiveDays;

    private boolean currentUser;
    private boolean activeUser;
    private AuthorizationModel authorization;

    @Ignore
    private boolean isComplete;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public RealmList<UserAwardModel> getAwards() {
        return awards;
    }

    @ParcelPropertyConverter(UserAwardModel.RealmListParcelConverter.class)
    public void setAwards(RealmList<UserAwardModel> awards) {
        this.awards = awards;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getRatingText() {
        return ratingText;
    }

    public void setRatingText(String ratingText) {
        this.ratingText = ratingText;
    }

    public String getRatingTotal() {
        return ratingTotal;
    }

    public void setRatingTotal(String ratingTotal) {
        this.ratingTotal = ratingTotal;
    }

    public String getRatingWeek() {
        return ratingWeek;
    }

    public void setRatingWeek(String ratingWeek) {
        this.ratingWeek = ratingWeek;
    }

    public String getTotalPostsNumber() {
        return totalPostsNumber;
    }

    public void setTotalPostsNumber(String totalPostsNumber) {
        this.totalPostsNumber = totalPostsNumber;
    }

    public String getGoodPostsNumber() {
        return goodPostsNumber;
    }

    public void setGoodPostsNumber(String goodPostsNumber) {
        this.goodPostsNumber = goodPostsNumber;
    }

    public String getBestPostsNumber() {
        return bestPostsNumber;
    }

    public void setBestPostsNumber(String bestPostsNumber) {
        this.bestPostsNumber = bestPostsNumber;
    }

    public String getCommentsNumber() {
        return commentsNumber;
    }

    public void setCommentsNumber(String commentsNumber) {
        this.commentsNumber = commentsNumber;
    }

    public boolean isCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(boolean currentUser) {
        this.currentUser = currentUser;
    }

    public boolean isActiveUser() {
        return activeUser;
    }

    public void setActiveUser(boolean activeUser) {
        this.activeUser = activeUser;
    }

    public AuthorizationModel getAuthorization() {
        return authorization;
    }

    public void setAuthorization(AuthorizationModel authorization) {
        this.authorization = authorization;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(String lastVisit) {
        this.lastVisit = lastVisit;
    }

    public String getConsecutiveDays() {
        return consecutiveDays;
    }

    public void setConsecutiveDays(String consecutiveDays) {
        this.consecutiveDays = consecutiveDays;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}