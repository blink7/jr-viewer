package com.blinkseven.jrviewer.db.model;

import org.parceler.Parcel;

import io.realm.AuthorizationModelRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@Parcel(implementations = {AuthorizationModelRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {AuthorizationModel.class})
@RealmClass
public class AuthorizationModel extends RealmObject {

    @PrimaryKey
    private String token;

    public AuthorizationModel() {
    }

    public AuthorizationModel(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}